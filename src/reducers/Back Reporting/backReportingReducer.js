export const GET_BACT_REPORTING_SUCCESS = 'GET_BACT_REPORTING_SUCCESS';
export const GET_BACT_REPORTING_FAIL = 'GET_BACT_REPORTING_FAIL';
const initialState = {
  backReporting: [],
  columns: []
};
export function backReportingData(state = initialState, action) {
  switch (action.type) {
    case GET_BACT_REPORTING_SUCCESS:
      return {
        ...state,
        backReporting: action.backReporting,
        columns: action.columns
      };
    case GET_BACT_REPORTING_FAIL:
      return {
        ...state,
        backReporting: action.backReporting,
        columns: action.columns
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}