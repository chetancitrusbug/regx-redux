export const GET_STACK_DATA_AVAILABLE = 'GET_STACK_DATA_AVAILABLE';
export const GET_STACK_DATA_NULL = 'GET_STACK_DATA_NULL';
const initialState = {
    stackData: [],
  };
  export function stackData(state = initialState, action) {
    switch (action.type) {
      case GET_STACK_DATA_AVAILABLE:
        return {
          ...state,
          stackData: action.stackData,
        };
      case GET_STACK_DATA_NULL:
        return {
          ...state,
          stackData: action.stackData,
        };
      // case userConstants.LOGOUT:
      //   return {};
      default:
        return state
    }
  }