export const GET_PIE_DATA_AVAILABLE = 'GET_PIE_DATA_AVAILABLE';
export const GET_PIE_DATA_NULL = 'GET_PIE_DATA_NULL';
const initialState = {
    getdata: [],
  };
  export function pieData(state = initialState, action) {
    switch (action.type) {
      case GET_PIE_DATA_AVAILABLE:
        return {
          ...state,
          getdata: action.getdata,
        };
      case GET_PIE_DATA_NULL:
        return {
          ...state,
          getdata: action.getdata,
        };
      // case userConstants.LOGOUT:
      //   return {};
      default:
        return state
    }
  }