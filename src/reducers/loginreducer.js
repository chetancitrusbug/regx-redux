export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';

const initialState = {
  loggedIn: false,
  user: ''
};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.userData,
      };
    case LOGIN_FAILURE:
      return {
        loggedIn: false,
        error: action.error,
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}