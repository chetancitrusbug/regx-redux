export const GET_DATA_FIRM_SUCCESS_FILTER = 'GET_DATA_FIRM_SUCCESS_FILTER';
export const GET_DATA_FIRM_FAIL_FILTER = 'GET_DATA_FIRM_FAIL_FILTER';
const initialState = {
  getdata: true,
  recouncilationdata: [],
  recouncilationColumns: []
};
export function filterRecouncilationFirm(state = initialState, action) {
  switch (action.type) {
    case GET_DATA_FIRM_SUCCESS_FILTER:
      return {
        ...state,
        getdata: true,
        recouncilationdata: action.recouncilationdata,
        recouncilationColumns: action.recouncilationColumns
      };
    case GET_DATA_FIRM_FAIL_FILTER:
      return {
        ...state,
        recouncilationdata: action.recouncilationdata,
        recouncilationColumns: action.recouncilationColumns
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}