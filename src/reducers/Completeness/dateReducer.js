export const TO_DATE = 'TO_DATE';
export const FROM_DATE = 'FROM_DATE';

const initialState = {
  toDate: new Date(),
};

export function dateReducer(state = initialState, action) {
  switch (action.type) {
    case TO_DATE:
      return {
        ...state,
        toDate: action.toDate,
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}