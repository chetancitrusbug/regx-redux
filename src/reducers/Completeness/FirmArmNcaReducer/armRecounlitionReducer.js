export const GET_DATA_ARM_SUCCESS = 'GET_DATA_ARM_SUCCESS';
export const GET_DATA_ARM_FAIL = 'GET_DATA_ARM_FAIL';
const initialState = {
  getdata: true,
  armrecouncilationdata: [],
  armrecouncilationColumns: []
};
export function armrecouncilationNca(state = initialState, action) {
  switch (action.type) {
    case GET_DATA_ARM_SUCCESS:
      return {
        ...state,
        getdata: true,
        armrecouncilationdata: action.armrecouncilationdata,
        armrecouncilationColumns: action.armrecouncilationColumns
      };
    case GET_DATA_ARM_FAIL:
      return {
        ...state,
        armrecouncilationdata: action.armrecouncilationdata,
        armrecouncilationColumns: action.armrecouncilationColumns
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}