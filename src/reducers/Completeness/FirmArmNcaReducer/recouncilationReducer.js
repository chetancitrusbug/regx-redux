export const GET_DATA_FIRM_SUCCESS = 'GET_DATA_FIRM_SUCCESS';
export const GET_DATA_FIRM_FAIL = 'GET_DATA_FIRM_FAIL';
const initialState = {
  getdata: true,
  recouncilationdata: [],
  recouncilationColumns: []
};
export function recouncilationNca(state = initialState, action) {
  switch (action.type) {
    case GET_DATA_FIRM_SUCCESS:
      return {
        ...state,
        getdata: true,
        recouncilationdata: action.recouncilationdata,
        recouncilationColumns: action.recouncilationColumns
      };
    case GET_DATA_FIRM_FAIL:
      return {
        ...state,
        recouncilationdata: action.recouncilationdata,
        recouncilationColumns: action.recouncilationColumns
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}