export const GET_EDIT_ARM_SUCCESS = 'GET_EDIT_ARM_SUCCESS';
export const GET_EDIT_ARM_FAIL = 'GET_EDIT_ARM_FAIL';
const initialState = {
  dataEditARM: []
};
export function editForArmNca(state = initialState, action) {
  switch (action.type) {
    case GET_EDIT_ARM_SUCCESS:
      return {
        ...state,
        dataEditARM: action.dataEditARM
      };
    case GET_EDIT_ARM_FAIL:
      return {
        ...state,
        dataEditARM: action.dataEditARM
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}