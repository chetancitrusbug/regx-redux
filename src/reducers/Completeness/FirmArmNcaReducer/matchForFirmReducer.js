export const GET_MATCH_FIRM_SUCCESS = 'GET_MATCH_FIRM_SUCCESS';
export const GET_MATCH_FIRM_FAIL = 'GET_MATCH_FIRM_FAIL';
const initialState = {
  getMatch: false
};
export function matchForFirmNca(state = initialState, action) {
  switch (action.type) {
    case GET_MATCH_FIRM_SUCCESS:
      return {
        ...state,
        getdata: true,
        getMatch: action.getMatch
      };
    case GET_MATCH_FIRM_FAIL:
      return {
        ...state,
        getMatch: action.getMatch
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}