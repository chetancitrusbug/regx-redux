export const GET_EDIT_FIRM_SUCCESS = 'GET_EDIT_FIRM_SUCCESS';
export const GET_EDIT_FIRM_FAIL = 'GET_EDIT_FIRM_FAIL';
const initialState = {
  dataEditFirm: []
};
export function editForFirmNca(state = initialState, action) {
  switch (action.type) {
    case GET_EDIT_FIRM_SUCCESS:
      return {
        ...state,
        dataEditFirm: action.dataEditFirm
      };
    case GET_EDIT_FIRM_FAIL:
      return {
        ...state,
        dataEditFirm: action.dataEditFirm
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}