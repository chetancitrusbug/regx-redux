export const GET_DATA_ARM_SUCCESS_FILTER = 'GET_DATA_ARM_SUCCESS_FILTER';
export const GET_DATA_ARM_FAIL_FILTER = 'GET_DATA_ARM_FAIL_FILTER';
const initialState = {
  getdata: true,
  armrecouncilationdata: [],
  armrecouncilationColumns: []
};
export function filterRecouncilationArmNca(state = initialState, action) {
  switch (action.type) {
    case GET_DATA_ARM_SUCCESS_FILTER:
      return {
        ...state,
        getdata: true,
        armrecouncilationdata: action.armrecouncilationdata,
        armrecouncilationColumns: action.armrecouncilationColumns
      };
    case GET_DATA_ARM_FAIL_FILTER:
      return {
        ...state,
        armrecouncilationdata: action.armrecouncilationdata,
        armrecouncilationColumns: action.armrecouncilationColumns
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}