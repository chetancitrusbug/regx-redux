export const GET_LIST_REPORTING_SUCCESS = 'GET_LIST_REPORTING_SUCCESS';
export const GET_LIST_REPORTING_FAIL = 'GET_LIST_REPORTING_FAIL';
const initialState = {
  client: [],
  columns: []
};
export function listData(state = initialState, action) {
  switch (action.type) {
    case GET_LIST_REPORTING_SUCCESS:
      return {
        ...state,
        client: action.client,
        columns: action.columns
      };
    case GET_LIST_REPORTING_FAIL:
      return {
        ...state,
        client: action.client,
        columns: action.columns
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}