export const GET_PERSONNEL_DATA_SUCCESS = 'GET_PERSONNEL_DATA_SUCCESS';
export const GET_PERSONNEL_DATA_FAIL = 'GET_PERSONNEL_DATA_FAIL';
const initialState = {
  getPersonnelValidation: [],
  columns: []
};
export function PersonnelData(state = initialState, action) {
  switch (action.type) {
    case GET_PERSONNEL_DATA_SUCCESS:
      return {
        ...state,
        getPersonnelValidation: action.getPersonnelValidation,
        columns: action.columns
      };
    case GET_PERSONNEL_DATA_FAIL:
      return {
        ...state,
        getPersonnelValidation: action.getPersonnelValidation,
        columns: action.columns
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}