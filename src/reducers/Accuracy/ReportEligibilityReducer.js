export const GET_REPORTING_SUCCESS = 'GET_REPORTING_SUCCESS';
export const GET_REPORTING_FAIL = 'GET_REPORTING_FAIL';
const initialState = {
  getReportingEligibility: [],
  columns: []
};
export function ReportingEligibilityData(state = initialState, action) {
  switch (action.type) {
    case GET_REPORTING_SUCCESS:
      return {
        ...state,
        getReportingEligibility: action.getReportingEligibility,
        columns: action.columns
      };
    case GET_REPORTING_FAIL:
      return {
        ...state,
        getReportingEligibility: action.getReportingEligibility,
        columns: action.columns
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}