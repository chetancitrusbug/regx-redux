export const GET_INSTRUMENTEL_DATA_SUCCESS = 'GET_INSTRUMENTEL_DATA_SUCCESS';
export const GET_INSTRUMENTEL_DATA_FAIL = 'GET_INSTRUMENTEL_DATA_FAIL';
const initialState = {
  getInstrumentValidation: [],
  columns: []
};
export function InstrumentData(state = initialState, action) {
  switch (action.type) {
    case GET_INSTRUMENTEL_DATA_SUCCESS:
      return {
        ...state,
        getInstrumentValidation: action.getInstrumentValidation,
        columns: action.columns
      };
    case GET_INSTRUMENTEL_DATA_FAIL:
      return {
        ...state,
        getInstrumentValidation: action.getInstrumentValidation,
        columns: action.columns
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}