export const GET_PRICING_DATA_SUCCESS = 'GET_PRICING_DATA_SUCCESS';
export const GET_PRICING_DATA_FAIL = 'GET_PRICING_DATA_FAIL';
const initialState = {
  getPricingValidation: [],
  columns: []
};
export function PricingData(state = initialState, action) {
  switch (action.type) {
    case GET_PRICING_DATA_SUCCESS:
      return {
        ...state,
        getPricingValidation: action.getPricingValidation,
        columns: action.columns
      };
    case GET_PRICING_DATA_FAIL:
      return {
        ...state,
        getPricingValidation: action.getPricingValidation,
        columns: action.columns
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}