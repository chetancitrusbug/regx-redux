import { combineReducers } from 'redux'
import { authentication } from './loginreducer';
//FirmArm reducers
import { recouncilation } from '../reducers/Completeness/FirmArmReducer/recouncilationReducer';
import { armrecouncilation } from '../reducers/Completeness/FirmArmReducer/armRecounlitionReducer';
import { filterRecouncilationArm } from '../reducers/Completeness/FirmArmReducer/filterRecouncilationArmReducer';
import { filterRecouncilationFirm } from '../reducers/Completeness/FirmArmReducer/filterRecouncilationFirmReducer';
import { matchForFirm } from '../reducers/Completeness/FirmArmReducer/matchForFirmReducer';
import { editForFirm } from '../reducers/Completeness/FirmArmReducer/editFormFirmReducer';
import { editForArm } from '../reducers/Completeness/FirmArmReducer/editForArm';

//data import reducers
import { dataImport } from '../reducers/Data Import/dataImportReducer';

//FirmArmNca reducers
import { recouncilationNca } from '../reducers/Completeness/FirmArmNcaReducer/recouncilationReducer';
import { armrecouncilationNca } from '../reducers/Completeness/FirmArmNcaReducer/armRecounlitionReducer';
import { filterRecouncilationFirmNca } from '../reducers/Completeness/FirmArmNcaReducer/filterRecouncilationFirmReducer';
import { filterRecouncilationArmNca } from '../reducers/Completeness/FirmArmNcaReducer/filterRecouncilationArmReducer';
import { matchForFirmNca } from '../reducers/Completeness/FirmArmNcaReducer/matchForFirmReducer';
import { editForFirmNca } from '../reducers/Completeness/FirmArmNcaReducer/editFormFirmReducer';
import { editForArmNca } from '../reducers/Completeness/FirmArmNcaReducer/editForArm';
import { dateReducer } from '../reducers/Completeness/dateReducer';

//Backreporting Reducer
import { backReportingData } from './Back Reporting/backReportingReducer';

//accuracy

import { ReportingEligibilityData } from './Accuracy/ReportEligibilityReducer';
import { InstrumentData } from './Accuracy/InstrumentDataReducer';
import { PricingData } from './Accuracy/PricingDataReducer';
import { PersonnelData } from './Accuracy/PersonnelDataReducer';

//List
import { listData } from './List/listDataReducer';

//Insight
import {pieData} from './Insight/pieDataReducer';
import {stackData} from './Insight/stackChartReducer';

export default combineReducers({
    authentication,
    recouncilation,
    armrecouncilation,
    filterRecouncilationFirm,
    filterRecouncilationArm,
    matchForFirm,
    editForFirm,
    editForArm,
    dataImport,
    recouncilationNca,
    armrecouncilationNca,
    filterRecouncilationFirmNca,
    filterRecouncilationArmNca,
    matchForFirmNca,
    editForFirmNca,
    editForArmNca,
    dateReducer,
    backReportingData,
    ReportingEligibilityData,
    InstrumentData,
    PricingData,
    PersonnelData,
    listData,
    pieData,
    stackData
})