export const GET_DATA_SUCCESS = 'GET_DATA_SUCCESS';
export const GET_DATA_FAIL = 'GET_DATA_FAIL';
const initialState = {
  getdata: [],
};
export function dataImport(state = initialState, action) {
  switch (action.type) {
    case GET_DATA_SUCCESS:
      return {
        ...state,
        getdata: action.getdata,
      };
    case GET_DATA_FAIL:
      return {
        ...state,
        getdata: action.getdata,
      };
    // case userConstants.LOGOUT:
    //   return {};
    default:
      return state
  }
}