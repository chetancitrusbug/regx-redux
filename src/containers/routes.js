import React from 'react';
import {HashRouter , Route,  Switch} from 'react-router-dom';
// import Demo from './demo';
import Login from '../components/auth/Login';
import ForgotPassword from '../components/auth/ForgotPassword';
// import ChangePassword from '../components/user/ChangePassword';
// //import Profile from '../components/user/Profile';
 import Home from '../components/UserModule/Home'
// import Upload from '../components/userModules/Upload'
 import Insights from '../components/UserModule/Insight/Insights'
// import AccuracyReport from '../components/userModules/AccuracyReport'
// import AccuracyInstrument from '../components/userModules/AccuracyInstrument'
 import FirmArm from '../components/UserModule/Completeness/FirmArm'
 import FirmArmNca from '../components/UserModule//Completeness/FirmArmNca'
 import DataImport from '../components/UserModule/Data Import/DataImport';
import BackReporting from '../components/UserModule/Back Reporting/BackReporting';
import ReportingEligibility from '../components/UserModule/Accuracy/ReportingEligibility.js';
import InstrumentData from '../components/UserModule/Accuracy/InstrumentData';
import PricingData from '../components/UserModule/Accuracy/PricingData';
import PersonnelData from '../components/UserModule/Accuracy/PersonnelData';
import superPerson from '../components/UserModule/Super People/superPerson';
import clientManager from '../components/UserModule/Client Manager/clientManager';
const Routes = () => (
<HashRouter >
    <Switch>
        {/* <Route exact path="/" component={Welcome}/> */}
            <Route path="/" exact component={Login}/>
        <Route exact path="/home" component={Home} />
        <Route path="/insights" exact component={Insights} />
        <Route path="/firm-arm" exact component={FirmArm} />
        <Route path="/data-import" exact component={DataImport} />
        <Route path="/firm-arm-nca" exact component={FirmArmNca} />
        <Route path="/backreporting" exact component={BackReporting} />
        <Route path="/ReportingEligibility" exact component={ReportingEligibility} />
        <Route path="/InstrumentData" exact component={InstrumentData} />
        <Route path="/PricingData" exact component={PricingData} />
        <Route path="/PersonnelData" exact component={PersonnelData} />
        <Route path="/forgot-password" component={ForgotPassword}/>
        <Route path="/superperson" component={superPerson} />
        <Route path="/clientmanager" component={clientManager} />
        
        {/* <Route exact path="/demo" component={Demo} /> */}
            {/* <Route path="/login" exact  component={Login}/>
           
            <Route path="/accuracy-report" exact component={AccuracyReport} />

            
            <Route path="/accuracy-instrument" exact component={AccuracyInstrument} />
           
            <Route path="/upload" exact  component={Upload}/>
       
        <Route path="/change-password" component={ChangePassword} /> */}
        {/* <Route path="/profile" component={Profile} /> */}
        {/* <Route path='/customer/edit/:empid' component={CustomerEdit} /> 
        <Route path='/customer/view/:empid' component={CustomerView} /> 
        <Route path='/offer/create/:custid' component={OfferCreate} />  */}
        {/* <Route path="*" component={NotFound}/> */}
            
    </Switch>
</HashRouter>
);
export default Routes;