import React, { Component } from 'react';
import '../utils/css/bootstrap.min.css'
import '../utils/css/icons.css'
import '../utils/css/style.css'
import Routes from './routes';

class App extends Component {
  constructor(){
    super();
    this.state={
      appName: "RegEx",
      home: false
    }
  }
  render() {
    return (
      
      <div className="App">
        <Routes name={this.state.appName}/>
      </div>
      
    );
  }
}

export default App;
