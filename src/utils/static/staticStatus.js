export const customerStatus = {
        0 : 'Lead',
        2 : 'Prospect',
        1 : 'Customer',
        3 : 'Lost'
}

export const sourceStatus = {
         0 : 'CRM',  // Our Website
        1 : 'API',  // get-party crone job
        2 : 'Website', // Wordpress Contact form
        3 : 'Device', // Mobile Api
    }
