import axios from 'axios';
import api from '../helpers/api';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
let userData = [];
let error = [];
export const userActions = {
    login
}
function login(user) {

    return dispatch => {
        axios.request({
            method: "post",
            url: api.url+api.login,
            crossDomain: true,
            data: user
        })
         .then((result) => {
            let responseJson = result;
         
            if(responseJson.data.API_Token){
           
                 
                    sessionStorage.setItem('userData', JSON.stringify(responseJson.data));
                    userData = responseJson.data;
                    dispatch(success(userData));          
              
                }else{
                    error = responseJson.data.status;
                    dispatch(failure(error));
                    alert('Username or Password Invalid');
                }        
        }).catch((error) =>{
            dispatch(failure(error));
            alert('Username or Password Invalid');
        });
    }
}
function success(userData) {
     return {
          type: LOGIN_SUCCESS, 
          userData,
        } 
    }
function failure(error) {
        return {
        type: LOGIN_FAILURE,
        error,
    }
}