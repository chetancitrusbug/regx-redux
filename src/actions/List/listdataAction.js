import axios from 'axios';
export const GET_LIST_REPORTING_SUCCESS = 'GET_LIST_REPORTING_SUCCESS';
export const GET_LIST_REPORTING_FAIL = 'GET_LIST_REPORTING_FAIL';
let client = [];
let columns = [];
export default function listDataAction(url){
    return dispatch => {
        axios.get(url).then(response => {
           
            
            if (response.data.status) {
                client =  response.data.result.client;
                columns =  response.data.result.columns;
                
                    dispatch(getListDataSuccess(client,columns));
            } else {
                client = [];
                columns = [];
                    dispatch(getListDataFail(client,columns));
               // alert(response.data.message)
            }
            
        }).catch(error => {
        
            client = error;
            dispatch(getListDataFail(client,columns));
        });
    }
}
function getListDataSuccess(client,columns) {
    return {
         type: GET_LIST_REPORTING_SUCCESS, 
         client:client,
         columns:columns,
       } 
   }

function getListDataFail(client,columns) {
    return {
         type: GET_LIST_REPORTING_FAIL, 
         client:client,
         columns:columns
       } 
   }
   