import axios from 'axios';
export const GET_INSTRUMENTEL_DATA_SUCCESS = 'GET_INSTRUMENTEL_DATA_SUCCESS';
export const GET_INSTRUMENTEL_DATA_FAIL = 'GET_INSTRUMENTEL_DATA_FAIL';
let getInstrumentValidation = [];
let columns = [];
export default function InstrumentDataAction(url){
    return dispatch => {
        axios.get(url).then(response => {
          
            
            if (response.data.status) {
               
                 
                getInstrumentValidation =  response.data.result.getInstrumentValidation;
                columns =  response.data.result.columns;
                
                    dispatch(getBackDataSuccess(getInstrumentValidation,columns));
            } else {
                getInstrumentValidation = [];
                columns = [];
                    dispatch(getBackDataFail(getInstrumentValidation,columns));
               // alert(response.data.message)
            }
            
        }).catch(error => {
   
            getInstrumentValidation = error;
            dispatch(getBackDataFail(getInstrumentValidation,columns));
        });
    }
}
function getBackDataSuccess(getInstrumentValidation,columns) {
    return {
         type: GET_INSTRUMENTEL_DATA_SUCCESS, 
         getInstrumentValidation:getInstrumentValidation,
         columns:columns,
       } 
   }

function getBackDataFail(getInstrumentValidation,columns) {
    return {
         type: GET_INSTRUMENTEL_DATA_FAIL, 
         getInstrumentValidation:getInstrumentValidation,
         columns:columns
       } 
   }
   