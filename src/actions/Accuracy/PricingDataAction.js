import axios from 'axios';
export const GET_PRICING_DATA_SUCCESS = 'GET_PRICING_DATA_SUCCESS';
export const GET_PRICING_DATA_FAIL = 'GET_PRICING_DATA_FAIL';
let getPricingValidation = [];
let columns = [];
export default function pricingDataAction(url){
    return dispatch => {
        axios.get(url).then(response => {
           
            
            if (response.data.status) {
            
                 
                getPricingValidation =  response.data.result.getPricingValidation;
                columns =  response.data.result.columns;
                
                    dispatch(getBackDataSuccess(getPricingValidation,columns));
            } else {
                getPricingValidation = [];
                columns = [];
                    dispatch(getBackDataFail(getPricingValidation,columns));
               // alert(response.data.message)
            }
            
        }).catch(error => {
    
            getPricingValidation = error;
            dispatch(getBackDataFail(getPricingValidation,columns));
        });
    }
}
function getBackDataSuccess(getPricingValidation,columns) {
    return {
         type: GET_PRICING_DATA_SUCCESS, 
         getPricingValidation:getPricingValidation,
         columns:columns,
       } 
   }

function getBackDataFail(getPricingValidation,columns) {
    return {
         type: GET_PRICING_DATA_FAIL, 
         getPricingValidation:getPricingValidation,
         columns:columns
       } 
   }
   