import axios from 'axios';
export const GET_PERSONNEL_DATA_SUCCESS = 'GET_PERSONNEL_DATA_SUCCESS';
export const GET_PERSONNEL_DATA_FAIL = 'GET_PERSONNEL_DATA_FAIL';
let getPersonnelValidation = [];
let columns = [];
export default function personnelDataAction(url){
    return dispatch => {
        axios.get(url).then(response => {
          
            
            if (response.data.status) {
               
                 
                getPersonnelValidation =  response.data.result.getPersonnelValidation;
                columns =  response.data.result.columns;
                
                    dispatch(getBackDataSuccess(getPersonnelValidation,columns));
            } else {
                getPersonnelValidation = [];
                columns = [];
                    dispatch(getBackDataFail(getPersonnelValidation,columns));
               // alert(response.data.message)
            }
            
        }).catch(error => {
          
            getPersonnelValidation = error;
            dispatch(getBackDataFail(getPersonnelValidation,columns));
        });
    }
}
function getBackDataSuccess(getPersonnelValidation,columns) {
    return {
         type: GET_PERSONNEL_DATA_SUCCESS, 
         getPersonnelValidation:getPersonnelValidation,
         columns:columns,
       } 
   }

function getBackDataFail(getPersonnelValidation,columns) {
    return {
         type: GET_PERSONNEL_DATA_FAIL, 
         getPersonnelValidation:getPersonnelValidation,
         columns:columns
       } 
   }
   