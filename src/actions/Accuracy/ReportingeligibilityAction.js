import axios from 'axios';
export const GET_REPORTING_SUCCESS = 'GET_REPORTING_SUCCESS';
export const GET_REPORTING_FAIL = 'GET_REPORTING_FAIL';
let getReportingEligibility = [];
let columns = [];
export default function reportEligibilityAction(url){
    return dispatch => {
        axios.get(url).then(response => {
           
            
            if (response.data.status) {
              
                 
                getReportingEligibility =  response.data.result.getReportingEligibility;
                columns =  response.data.result.columns;
                
                    dispatch(getBackDataSuccess(getReportingEligibility,columns));
            } else {
                getReportingEligibility = [];
                columns = [];
                    dispatch(getBackDataFail(getReportingEligibility,columns));
               // alert(response.data.message)
            }
            
        }).catch(error => {
           
            getReportingEligibility = error;
            dispatch(getBackDataFail(getReportingEligibility,columns));
        });
    }
}
function getBackDataSuccess(getReportingEligibility,columns) {
    return {
         type: GET_REPORTING_SUCCESS, 
         getReportingEligibility:getReportingEligibility,
         columns:columns,
       } 
   }

function getBackDataFail(backReporting) {
    return {
         type: GET_REPORTING_FAIL, 
         getReportingEligibility:getReportingEligibility,
         columns:columns
       } 
   }
   