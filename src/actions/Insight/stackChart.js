import axios from 'axios';
export const GET_STACK_DATA_AVAILABLE = 'GET_STACK_DATA_AVAILABLE';
export const GET_STACK_DATA_NULL = 'GET_STACK_DATA_NULL';
let stackData = [];
export default function stackChart(url){
    return dispatch => {
        axios.get(url).then(response => {
            if(response.data.code === 200){
                if(response.data.status === true){
                    stackData = response.data.result;
                    dispatch(getStackDataAvi(stackData));
                }else{
                    stackData = response.data.result;
                    dispatch(getStackDataNull(stackData));
                }
            }
        }).catch(error => {
           alert(error);
        });
    }
}
function getStackDataAvi(stackData) {
    return {
         type: GET_STACK_DATA_AVAILABLE, 
         stackData:stackData,
       } 
   }
   function getStackDataNull(stackData) {
    return {
         type: GET_STACK_DATA_NULL, 
         stackData:stackData,
       } 
   }