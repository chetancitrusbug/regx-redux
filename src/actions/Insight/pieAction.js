import axios from 'axios';
export const GET_PIE_DATA_AVAILABLE = 'GET_PIE_DATA_AVAILABLE';
export const GET_PIE_DATA_NULL = 'GET_PIE_DATA_NULL';
let getdata = [];
export default function pieDataAction(url){
    return dispatch => {
        axios.get(url).then(response => {
            if(response.data.code === 200){
                if(response.data.status === true){
                    getdata = response.data.result;
                    dispatch(getPieDataAvi(getdata));
                }else{
                    getdata = response.data.result;
                    dispatch(getPieDataNull(getdata));
                }
            }
        }).catch(error => {
           alert(error);
        });
    }
}
function getPieDataAvi(getdata) {
    return {
         type: GET_PIE_DATA_AVAILABLE, 
         getdata:getdata,
       } 
   }
   function getPieDataNull(getdata) {
    return {
         type: GET_PIE_DATA_NULL, 
         getdata:getdata,
       } 
   }