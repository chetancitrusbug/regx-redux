import axios from 'axios';
import api from '../../../helpers/api';
export const GET_EDIT_FIRM_SUCCESS = 'GET_EDIT_FIRM_SUCCESS';
export const GET_EDIT_FIRM_FAIL = 'GET_EDIT_FIRM_FAIL';
let dataEditFirm = [];
export default function editForFirmNca(user){
        return dispatch => {
            axios.request({
                method: "post",
                url: api.url+api.updateRecouncilationDetail,
                crossDomain: true,
                data: user
            }).then((result) => {
                let responseJson = result;
                if(responseJson.data){
                    if (responseJson.status === 200){
               alert('Edit Updated Successfully');
               dataEditFirm = responseJson.config.data;
               dispatch(getEditForFirmSuccess(dataEditFirm));
            }else{
                dataEditFirm = responseJson.config.data;
                dispatch(getEditForFirmFAIL(dataEditFirm));    
            }        
     }}).catch(error => {
             
           });  
    }
}
function getEditForFirmSuccess(getMatch) {
    return {
            type: GET_EDIT_FIRM_SUCCESS, 
            dataEditFirm:dataEditFirm
        } 
    }
    function getEditForFirmFAIL(getMatch) {
    return {
            type: GET_EDIT_FIRM_FAIL, 
            dataEditFirm:dataEditFirm
        } 
    }
