import axios from 'axios';
export const GET_MATCH_FIRM_SUCCESS = 'GET_MATCH_FIRM_SUCCESS';
export const GET_MATCH_FIRM_FAIL = 'GET_MATCH_FIRM_FAIL';
let getMatch;
export default function matchForFirmNca(url){
        return dispatch => {
            axios.post(url).then(response => {
                // alert(response.data.message)
            if(response.data.status === true){
             
                getMatch=true;
                dispatch(getMatchForFirmSuccess(getMatch));
            }else{
                getMatch=false;
            }
            }).catch(error => {
        
            dispatch(getMatchForFirmFAIL(getMatch));
            });  
        }
    }
function getMatchForFirmSuccess(getMatch) {
    return {
            type: GET_MATCH_FIRM_SUCCESS, 
            getMatch:getMatch
        } 
    }
    function getMatchForFirmFAIL(getMatch) {
    return {
            type: GET_MATCH_FIRM_FAIL, 
        getMatch:getMatch
        } 
    }
