import axios from 'axios';
export const GET_DATA_FIRM_SUCCESS = 'GET_DATA_FIRM_SUCCESS';
export const GET_DATA_FIRM_FAIL = 'GET_DATA_FIRM_FAIL';

let recouncilationdata = [];
let recouncilationColumns = [];
export default function recouncilationNca(url){
    return dispatch => {
        axios.get(url).then(response => {
            if (response.data.status) {
                     recouncilationdata =  response.data.result.recouncilation;
                    recouncilationColumns = response.data.result.columns;
                    dispatch(getrecouncilationdata(recouncilationdata,recouncilationColumns));
            } else {
                    recouncilationdata= [];
                    recouncilationColumns= [];
                    dispatch(getrecouncilationdataFail(recouncilationdata,recouncilationColumns));
               // alert(response.data.message)
            }
        }).catch(error => {
         
        });
    }
}
function getrecouncilationdata(recouncilationdata,recouncilationColumns) {
    return {
         type: GET_DATA_FIRM_SUCCESS, 
         recouncilationdata:recouncilationdata,
         recouncilationColumns:recouncilationColumns
       } 
   }
   function getrecouncilationdataFail(recouncilationdata,recouncilationColumns) {
    return {
         type: GET_DATA_FIRM_FAIL, 
        recouncilationdata:recouncilationdata,
        recouncilationColumns:recouncilationColumns
       } 
   }