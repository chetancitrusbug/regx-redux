import axios from 'axios';
export const GET_DATA_ARM_SUCCESS = 'GET_DATA_ARM_SUCCESS';
export const GET_DATA_ARM_FAIL = 'GET_DATA_ARM_FAIL';

let armrecouncilationdata = [];
let armrecouncilationColumns = [];
export default function armrecouncilation(url){
    return dispatch => {
        axios.get(url).then(response => {
            if (response.data.status) {
                    armrecouncilationdata =  response.data.result.recouncilation;
                    armrecouncilationColumns = response.data.result.columns;
                    dispatch(getrecouncilationdata(armrecouncilationdata,armrecouncilationColumns));
            } else {
                    armrecouncilationdata= [];
                    armrecouncilationColumns= [];
                    dispatch(getrecouncilationdataFail(armrecouncilationdata,armrecouncilationColumns));
               // alert(response.data.message)
            }
        }).catch(error => {
          
        });
    }
}
function getrecouncilationdata(armrecouncilationdata,armrecouncilationColumns) {
    return {
         type: GET_DATA_ARM_SUCCESS, 
         armrecouncilationdata:armrecouncilationdata,
         armrecouncilationColumns:armrecouncilationColumns
       } 
   }
   function getrecouncilationdataFail(recouncilationdata,recouncilationColumns) {
    return {
         type: GET_DATA_ARM_FAIL, 
        armrecouncilationdata:armrecouncilationdata,
        armrecouncilationColumns:armrecouncilationColumns
       } 
   }