import axios from 'axios';
import api from '../../../helpers/api';
export const GET_EDIT_ARM_SUCCESS = 'GET_EDIT_ARM_SUCCESS';
export const GET_EDIT_ARM_FAIL = 'GET_EDIT_ARM_FAIL';
let dataEditARM = [];
export default function editForArm(user){
        return dispatch => {
            axios.request({
                method: "post",
                url: api.url+api.updateRecouncilationDetail,
                crossDomain: true,
                data: user
            }).then((result) => {
                let responseJson = JSON.parse(result);
                  
                    setTimeout(() => {
        if (responseJson.status === true){
               alert('Edit Updated Successfully');
               dataEditARM = responseJson.config.data;
               dispatch(getEditForFirmSuccess(dataEditARM));
            }else{
                dataEditARM = responseJson.config.data;
                dispatch(getEditForFirmFAIL(dataEditARM));    
            }       
                }, 2000);
                     
    }).catch(error => {
            
           });  
    }
}
function getEditForFirmSuccess(getMatch) {
    return {
            type: GET_EDIT_ARM_SUCCESS, 
            dataEditArm:dataEditARM
        } 
    }
    function getEditForFirmFAIL(getMatch) {
    return {
            type: GET_EDIT_ARM_FAIL, 
            dataEditArm:dataEditARM
        } 
    }
