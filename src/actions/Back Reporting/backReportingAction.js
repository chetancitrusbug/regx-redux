import axios from 'axios';
export const GET_BACT_REPORTING_SUCCESS = 'GET_BACT_REPORTING_SUCCESS';
export const GET_BACT_REPORTING_FAIL = 'GET_BACT_REPORTING_FAIL';
let backReporting = [];
let columns = [];
export default function backReportingAction(url){
    return dispatch => {
        axios.get(url).then(response => {
           
            
            if (response.data.status) {
                backReporting =  response.data.result.backReporting;
                columns =  response.data.result.columns;
                
                    dispatch(getBackDataSuccess(backReporting,columns));
            } else {
                backReporting = [];
                columns = [];
                    dispatch(getBackDataFail(backReporting,columns));
               // alert(response.data.message)
            }
            
        }).catch(error => {
           
            backReporting = error;
            dispatch(getBackDataFail(backReporting,columns));
        });
    }
}
function getBackDataSuccess(backReporting,columns) {
    return {
         type: GET_BACT_REPORTING_SUCCESS, 
         backReporting:backReporting,
         columns:columns,
       } 
   }

function getBackDataFail(backReporting) {
    return {
         type: GET_BACT_REPORTING_FAIL, 
         backReporting:backReporting,
         columns:columns
       } 
   }
   