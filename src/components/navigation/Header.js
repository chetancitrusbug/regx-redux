import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
// import logo from '../static/images/logo.png';
import Logo from '../../utils/images/logo.png';
import Logosm from '../../utils/images/logo-sm.png';
import DummyUser from '../../utils/images/users/user-4.jpg';
import { Link } from 'react-router-dom';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            isNotification: false,
            active: this.props.activeClass,
            isOpenSetUp: false,
            userData: JSON.parse(sessionStorage.getItem('userData'))
        };
    }
    handleLogout = () => {
        sessionStorage.removeItem('userData');
    }
    openNavBar = () => {
        this.setState({ isOpen: !this.state.isOpen });
    }
    openNotificationBar = () => {
        this.setState({ isNotification: !this.state.isNotification });
    }
    openSetUp = () => {
        this.setState({ isOpenSetUp: !this.state.isOpenSetUp });
    }
    render() {
        let userData = [];
        userData = JSON.parse(sessionStorage.getItem('userData'));

        if (!sessionStorage.getItem('userData')) {
            return <Redirect exact push to={{ pathname: '/' }} />
        }
        return (
            <div>
                <header id="topnav">
                    <div className="topbar-main">
                        <div className="container-fluid">
                            {/* <!-- Logo container--> */}
                            <div className="logo-div">
                                <Link to="/insights" className="logo">
                                    <img src={Logosm} alt="" className="logo-small" />
                                    <img src={Logo} alt="" className="logo-large" />
                                </Link>
                            </div>
                            <div className="main-nav clearfix">
                                <ul>
                                    <li><Link to="#" className="active">MiFD II</Link></li>
                                    <li><Link to="#">EMIR</Link></li>
                                    <li><Link to="#">Dodd Frank</Link></li>
                                    {userData.label === "SP" ?
                                        <li><Link to="#">Setup</Link></li>
                                        :
                                        <li><Link to="#" onClick={this.openSetUp.bind(this)}>Setup</Link>
                                            <div className={`dropdown-menu dropdown-menu-right profile-dropdown ${this.state.isOpenSetUp ? 'show' : ''}`}>
                                                {userData.label === "SP" ? null :
                                                    <ul>
                                                        {userData.label === "RA" ?
                                                            <li><Link to="/clientmanager" className={`${this.state.active == 'clientmanager' ? 'active' : ''}`}>Client Manager</Link></li>
                                                            : null
                                                        }
                                                        <li><Link to="/superperson" className={`${this.state.active == 'superperson' ? 'active' : ''}`}>Support Person</Link> </li>
                                                    </ul>
                                                }
                                            </div>
                                        </li>
                                    }
                                </ul>
                            </div>

                            <div className="menu-extras topbar-custom">
                                <ul className="float-right list-unstyled mb-0">


                                    {/* <li className=" notification-list">
                                    <a className="nav-link "  href="#" ><i className="regx-message nav-icon"></i></a>
                                </li>
                           
                                <li className={`dropdown notification-list ${this.state.isNotification ? 'show' : ''}`}>
       
                                    <a className="nav-link dropdown-toggle arrow-none waves-effect" onClick={()=>this.openNotificationBar()} data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                        <i className="regx-bell noti-icon  nav-icon"></i> 
                                        <span className="badge badge-pill badge-danger noti-icon-badge">3</span>
                                    </a>
                                    <div className={`dropdown-menu dropdown-menu-right dropdown-menu-lg ${this.state.isNotification ? 'show' : ''}`}>
                                  
                                        <h6 className="dropdown-item-text">Notifications (258)</h6>
                                        <div className="slimscroll notification-item-list">
                                       
                                            <a href="javascript:void(0);" className="dropdown-item notify-item active">
                                                <div className="notify-icon bg-success"><i className="mdi mdi-cart-outline"></i></div>
                                                <p className="notify-details">Your order is placed<span className="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                            </a>
                                     
                                            <a href="javascript:void(0);" className="dropdown-item notify-item">
                                                <div className="notify-icon bg-warning"><i className="mdi mdi-message"></i></div>
                                                <p className="notify-details">New Message received<span className="text-muted">You have 87 unread messages</span></p>
                                            </a>
                                     
                                            <a href="javascript:void(0);" className="dropdown-item notify-item">
                                                <div className="notify-icon bg-info"><i className="mdi mdi-martini"></i></div>
                                                <p className="notify-details">Your item is shipped<span className="text-muted">It is a long established fact that a reader will</span></p>
                                            </a>
                                
                                            <a href="javascript:void(0);" className="dropdown-item notify-item">
                                                <div className="notify-icon bg-primary"><i className="mdi mdi-cart-outline"></i></div>
                                                <p className="notify-details">Your order is placed<span className="text-muted">Dummy text of the printing and typesetting industry.</span></p>
                                            </a>
                              
                                            <a href="javascript:void(0);" className="dropdown-item notify-item">
                                                <div className="notify-icon bg-danger"><i className="mdi mdi-message"></i></div>
                                                <p className="notify-details">New Message received<span className="text-muted">You have 87 unread messages</span></p>
                                            </a>
                                        </div>
                                
                                        <a href="javascript:void(0);" className="dropdown-item text-center text-primary">View all <i className="fi-arrow-right"></i></a>
                                </div>
                            </li>
                            
                            <li className=" notification-list">
                                <a className="nav-link " href="#" ><i className="regx-help  nav-icon"></i></a>
                            </li> */}
                                    <li className="dropdown notification-list">
                                        <div className={`dropdown notification-list nav-pro-img ${this.state.isOpen ? 'show' : ''}`}>
                                            <Link className={`dropdown-toggle nav-link arrow-none waves-effect nav-user `} onClick={() => this.openNavBar()} data-toggle="dropdown" to="#" role="button" aria-haspopup="false" aria-expanded="false">
                                                <img src={DummyUser} alt="user" className="rounded-circle" />
                                            </Link>
                                            <div className={`dropdown-menu dropdown-menu-right profile-dropdown ${this.state.isOpen ? 'show' : ''}`} style={{ width: "185px" }}>
                                                {/* <!-- item-->  */}
                                                <Link className="dropdown-item" to="#" style={{ display: "-webkit-inline-box" }}>
                                                    <img src={DummyUser} alt="user" width="40px" height="40px" />
                                                    <ul>
                                                        <li>{this.state.userData.UserName}</li>
                                                        <li> {this.state.userData.ClientName}</li>
                                                    </ul>
                                                </Link>

                                                {/* <a className="dropdown-item" href="#">
                                            <i className="mdi mdi-wallet m-r-5"></i> My Wallet
                                        </a> 
                                        <a className="dropdown-item d-block" href="#">
                                            <span className="badge badge-success float-right">11</span>
                                            <i className="mdi mdi-settings m-r-5"></i> Settings
                                        </a> 
                                        <a className="dropdown-item" href="#">
                                            <i className="mdi mdi-lock-open-outline m-r-5"></i> Lock screen
                                        </a> */}
                                                <div className="dropdown-divider"></div>
                                                <Link className="dropdown-item text-danger" onClick={() => this.handleLogout()} to="#"><i className="mdi mdi-power text-danger"></i> Logout</Link>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="menu-item">
                                        {/* <!-- Mobile menu toggle-->  */}
                                        <Link to="#" className="navbar-toggle nav-link" id="mobileToggle">
                                            <div className="lines"><span></span> <span></span> <span></span></div>
                                        </Link>
                                        {/* <!-- End mobile menu toggle--> */}
                                    </li>
                                </ul>
                            </div>
                            {/* <!-- end menu-extras --> */}
                            <div className="clearfix"></div>
                        </div>
                        {/* <!-- end container --> */}
                    </div>
                    {/* <!-- end topbar-main --> */}


                    {/* <!-- MENU Start --> */}
                    <div className="navbar-custom">
                        <div className="container-fluid">
                            <div id="navigation">
                                {/* <!-- Navigation Menu--> */}


                                {userData.label === "SP" ?
                                    <ul className="navigation-menu">
                                        <li className="has-submenu">
                                            <Link to="/insights" className={`${this.state.active == 'insights' ? 'active' : ''}`} ><span>Insights</span></Link>
                                        </li>
                                        <li><Link to="/data-import" className={`${this.state.active == 'data-import' ? 'active' : ''}`}>Data Import</Link></li>
                                        <li className="has-submenu"><Link to="/backreporting" className={`${this.state.active == 'backreporting' ? 'active' : ''}`}>Reporting</Link> </li>
                                        
                                        <li className={`has-submenu ${[
                                            "firmArm",
                                            "firmArmNca"
                                        ].indexOf(this.state.active) >= 0 ? 'active' : null}`}><Link to="#">Completeness</Link>
                                            <ul className="submenu">
                                                <li className={`${this.state.active == 'firmArm' ? 'active' : ''}`}>
                                                    <Link to="/firm-arm">Firm-ARM Reconciliation</Link>
                                                </li>
                                                <li className={`${this.state.active == 'firmArmNca' ? 'active' : ''}`}>
                                                    <Link to="/firm-arm-nca">Firm-ARM-NCA Reconciliation</Link>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className={`has-submenu ${[
                                            "accuracyReport",
                                            "accuracyInstrument",
                                            "pricingData",
                                            "accuracyPersonnel"
                                        ].indexOf(this.state.active) >= 0 ? 'active' : null}`}><Link to="#">Accuracy</Link>
                                            <ul className="submenu">
                                                <li className={`${this.state.active == 'accuracyReport' ? 'active' : ''}`}><Link to="/ReportingEligibility" >Reporting Eligibility</Link></li>
                                                <li className={`${this.state.active == 'accuracyInstrument' ? 'active' : ''}`}><Link to="/InstrumentData" >Instrument Data</Link></li>
                                                <li className={`${this.state.active == 'pricingData' ? 'active' : ''}`}><Link to="/PricingData">Pricing Data </Link></li>
                                                <li className={`${this.state.active == 'accuracyPersonnel' ? 'active' : ''}`}><Link to="/PersonnelData">Personnel Data </Link></li>
                                            </ul>
                                        </li>    
                                    </ul>
                                    :
                                    <ul className="navigation-menu">
                                        <li className="has-submenu">
                                            <Link to="/insights" className={`${this.state.active == 'insights' ? 'active' : ''}`} ><span>Insights</span></Link>
                                        </li>
                                        <li><Link to="/data-import" className={`${this.state.active == 'data-import' ? 'active' : ''}`}>Data Import</Link></li>
                                        <li className="has-submenu"><Link to="/backreporting" className={`${this.state.active == 'backreporting' ? 'active' : ''}`}>Reporting</Link> </li>
                                        <li className={`has-submenu ${[
                                            "firmArm",
                                            "firmArmNca"
                                        ].indexOf(this.state.active) >= 0 ? 'active' : null}`}><Link to="#">Completeness</Link>
                                            <ul className="submenu">
                                                <li className={`${this.state.active == 'firmArm' ? 'active' : ''}`}>
                                                    <Link to="/firm-arm">Firm-ARM Reconciliation</Link>
                                                </li>
                                                <li className={`${this.state.active == 'firmArmNca' ? 'active' : ''}`}>
                                                    <Link to="/firm-arm-nca">Firm-ARM-NCA Reconciliation</Link>
                                                </li>
                                            </ul>
                                        </li>
                                        <li className={`has-submenu ${[
                                            "accuracyReport",
                                            "accuracyInstrument",
                                            "pricingData",
                                            "accuracyPersonnel"
                                        ].indexOf(this.state.active) >= 0 ? 'active' : null}`}><Link to="#">Accuracy</Link>
                                            <ul className="submenu">
                                                <li className={`${this.state.active == 'accuracyReport' ? 'active' : ''}`}><Link to="/ReportingEligibility" >Reporting Eligibility</Link></li>
                                                <li className={`${this.state.active == 'accuracyInstrument' ? 'active' : ''}`}><Link to="/InstrumentData" >Instrument Data</Link></li>
                                                <li className={`${this.state.active == 'pricingData' ? 'active' : ''}`}><Link to="/PricingData">Pricing Data </Link></li>
                                                <li className={`${this.state.active == 'accuracyPersonnel' ? 'active' : ''}`}><Link to="/PersonnelData">Personnel Data </Link></li>
                                            </ul>
                                        </li>
                                    </ul>
                                }
                                {/* <!-- End navigation menu --> */}
                            </div>
                            {/* <!-- end navigation --> */}
                        </div>
                        {/* <!-- end container-fluid --> */}
                    </div>
                    {/* <!-- end navbar-custom --> */}
                </header>
            </div>

        )
    }
};


export default Header;