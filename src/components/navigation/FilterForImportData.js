import React, { Component } from 'react';
import { Button, Modal } from 'react-bootstrap';
import api from '../../helpers/api';
import axios from 'axios';
import {Redirect} from 'react-router-dom';
class FilterForImportData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            isNotification: false,
            active: this.props.activeClass,
            showModal: false,
            showModalClass: '',
            userData: JSON.parse(sessionStorage.getItem('userData')),
            clientList: [],
            clientListNext: [],
            listFiles: [],
            listFile: 'false',
            clientType: [],
            ClientTypeName: '',
            transpose: true,
            refresh: true,
            showModal: false, 
            showModalClass: '',
            isDisable: true,
            redirectToFerrer:false
        };
        this.handleShowModal = this.handleShowModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.fileUpload = this.fileUpload.bind(this);
        this.handleUserInput = this.handleUserInput.bind(this);
    
    };
    handleCloseModal() {
        this.setState({ showModal: false, showModalClass: '' });
    }
    handleShowModal() {
        this.setState({ showModal: true, showModalClass: 'show' });
    }
    handleTrans(e) {
        const target = e.target;
        this.setState({ transpose: target.value });
        this.props.ontransClick(this.state.transpose);
        if (this.state.transpose === true) {
            this.setState({ transpose: false });
            this.props.ontransClick(this.state.transpose);
        } else {
            this.setState({ transpose: true });
            this.props.ontransClick(this.state.transpose);
        }
    }
    handleRefresh(e) {
        const target = e.target;
        this.setState({ refresh: target.value });
        this.props.onRefreshClick(this.state.refresh);
        if (this.state.refresh === true) {
            this.setState({ refresh: false });
            this.props.onRefreshClick(this.state.refresh);
        } else {
            this.setState({ refresh: true });
            this.props.onRefreshClick(this.state.refresh);
        }
    }
    handleUserInput(event) {
        const name = event.target.name;
        const value = event.target.value;
        //this.getClient(e.target.name, e.target.value)
        this.setState({ [name]: value });

    }
    onFormSubmit(e) {
        e.preventDefault() // Stop form submit
   
        if (this.state.ClientTypeName === '') {
            alert('Must select client type');
        } else {
            if (this.state.file) {
                this.fileUpload(this.state.file);
            } else {
                alert('File must be in .csv formate');
            }
        }
    }
    onChange(e) {
        if (e.target.files[0].type === "application/vnd.ms-excel" || e.target.files[0].type === "text/csv") {
            this.setState({ file: e.target.files[0], isDisable: false })


        } else {
            alert('File must be in .csv formate');
        }
    }

    fileUpload(file) {
        var formData = new FormData();
        formData.append("file", file);
        formData.append("api_token", this.state.userData.API_Token);
        formData.append("ClientName", JSON.parse(sessionStorage.getItem('userData')).ClientName);
        formData.append("clientType", this.state.ClientTypeName);
        let url = api.url + api.uploadFiles
        
        axios.post(url, formData, {
            headers: { 'Content-Type': 'multipart/form-data;text/html' }
        }).then(response => {
            if(response.status === 200){
                if (response.data.status) {
                 
                    alert('File Uploaded')
                    this.handleCloseModal();
                } else {
                    alert(response.data.message)
                
                }
            }else{
                alert('Please Login First')
                sessionStorage.removeItem('userData');
                this.setState({redirectToFerrer:true});
            }
                }).catch(error => {


        });
    }
    handleRefresh(e) {
        const target = e.target;
        this.setState({ refresh: target.value });
        this.props.onRefreshClick(this.state.refresh);
        if (this.state.refresh === true) {
            this.setState({ refresh: false });
            this.props.onRefreshClick(this.state.refresh);
        } else {
            this.setState({ refresh: true });
            this.props.onRefreshClick(this.state.refresh);
        }
    }
    componentWillMount() {
        this.setState({ loading: false });
        let url = api.url + api.getClientType + '?api_token=' + this.state.userData.API_Token;
        axios.get(url).then(response => {


            this.setState({ loading: true });
            this.setState({
                clientType: response.data.result
            });
        }).catch(error => {


        });
    }
    render() {
        if(this.state.redirectToFerrer === true){
            return <Redirect exact push to={{ pathname: '/' }} />
        }
        return (
            <div className="col-xl-6 col-sm-6 pull-right">
                <div className="filter-search-div">
                    <div className="filter-nav-div ">
                        <div className="filt-nav-ul clearfix">
                            <ul>
                                <li>

                                    <label onClick={this.handleRefresh.bind(this)}
                                        value={this.props.refresh} className="ion-refresh arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Refresh" title="Refresh"></label>

                                </li>
                                <li>

                                    {/* <input type="file" id="file1" name="image" accept="image/*" capture style={{"display":"none"}}/> */}
                                    <span onClick={this.handleShowModal} className="ion-upload arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Upload" title="Upload"></span>

                                </li>
                                {/* <li>
                                    <label onClick={this.handleRefresh.bind(this)}
                                        value={this.props.refresh} className="ion-refresh arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Refresh" title="Refresh"></label>
                                </li> */}
                                {/* <!--li><a href="#"><span className="ion-search" data-toggle="tooltip" data-placement="top" data-title="Search" title="Search"></span></a></li--> */}
                                {/* <li><a href="#"><span className="ion-android-sort" data-toggle="tooltip" data-placement="top" data-title="Sort" title="Sort"></span></a></li> */}
                                <li>
                                    <label onClick={this.handleTrans.bind(this)}
                                        value={this.state.transpose} className="ion-android-arrow-down-right arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Transpose" title="Transpose"></label>
                                </li>

                            </ul>
                        </div>
                    </div>
                    {/* <!-- end of filter-nav-div--> */}
                    <div className="search-data-div">
                        <div className="clearfix">
                            <form>
                                <div className="search-col-1"><input type="text" className="form-control" placeholder="Search" /></div>
                                <div className="search-col-6"><button className="btn sbtn">Search</button></div>
                            </form>
                        </div>
                    </div>
                </div>
                {/* <!-- end of filter-search-div --> */}
                <Modal className={`${this.state.showModalClass}`} show={this.state.showModal} onHide={this.handleCloseModal}>
                    <Modal.Header className="modal-header1" closeButton>
                        <Modal.Title className="title-files">Upload Files</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="container">
                            <div className="demoForm">
                                {!this.state.loading ? <div><i className="fa fa-spin fa-fw fa-spinner"></i> Loding</div> :
                                    <form onSubmit={this.onFormSubmit}>
                                        {/* <div className={`form-group col-md-12 radio-root1 clearfix`}>
                                            <label className="col-md-12 padd-0 clearfix " htmlFor="email">Reconciliation Type</label>
                                            
                                            <div className="radio-inline clearfix">
                                                <span forname="Firm-ARM" className="radio-span1">Firm-ARM </span> 
                                                <input type="radio" id="Firm-ARM"
                                                 onChange={this.handleUserInput} name="recouncilationType" className=" form-control radio-input" value={this.state.firmArm} />
                                            </div>
                                            <div className="radio-inline clearfix">
                                                <span className="radio-span1" forname="Firm-ARM-NCA">Firm-ARM-NCA </span>
                                                <input type="radio" id="Firm-ARM-NCA" 
                                                onChange={this.handleUserInput} name="recouncilationType" className=" form-control radio-input" value={this.state.firmArmNca} />
                                            </div>

                                        </div> */}
                                        <div className="form-group clearfix">
                                            <label htmlFor="state" className="col-md-12 control-label clearfix">Client Type </label>
                                            <div className="col-md-12 clearfix">
                                                {/* {ClientTypes} */}
                                                {
                                                    this.state.clientType.map((option, i) => {
                                                        return <label key={i}>
                                                            <input
                                                                type="radio"
                                                                name="ClientTypeName"
                                                                key={i}
                                                                onChange={this.handleUserInput}
                                                                value={option.ClientTypeName} />
                                                            {option.ClientTypeName}
                                                        </label>
                                                    })
                                                }
                                            </div>

                                        </div>
                                        <div className={`form-group col-md-12 clearfix`}>
                                            <label className="col-md-12 padd-0 clearfix" htmlFor="email">File Upload</label>
                                            <input type="file" onChange={this.onChange} className=" form-control input-file" />
                                            <label>Saved your file as fromClient_toClient_Date.csv</label><br />
                                            <label>Example:-Schroders_UnaVista_31-01-2019.csv</label><br />
                                            <label>File type must be in csv formate</label>
                                        </div>
                                        {/* <div className="form-group">
                                            <label htmlFor="state" className="col-md-2 control-label">From </label>
                                            <div className="col-md-6 ">
                                                <select className="form-control" defaultValue='' onChange={(event) => this.handleUserInput(event)} required="" id="from" name="from">
                                                    <option value=''>Select</option>
                                                    {allProfiles}
                                                </select>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="state" className="col-md-2 control-label">To </label>
                                            <div className="col-md-6">
                                                <select className="form-control" defaultValue='' onChange={(event) => this.handleUserInput(event)} required="" id="to" name="to">
                                                    <option value=''>Select</option>
                                                    {allClients}
                                                </select>
                                            </div>
                                        </div> */}
                                        <div className="col-md-9">
                                            <button type="submit" className="btn btn-primary " disabled={this.state.isDisable}>Upload</button>
                                        </div>
                                    </form>
                                }
                                {/* <div className="cols col-md-3">
                        <a href="/forgot-password" className="btn btn-primary">Forgot Password</a>
                    </div> */}

                            </div>



                        </div>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleCloseModal}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>

        )
    }
};
export default FilterForImportData;

