import React, { Component } from 'react';
import api from '../../helpers/api'
import axios from 'axios'
import { connect } from 'react-redux';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { Button, Modal } from 'react-bootstrap';
import { formatDate, parseDate } from 'react-day-picker/moment';
import filterRecouncilationFirmNca from '../../actions/Completeness/FirmArmNcaAction/filterRecouncilationFirmAction';
import filterRecouncilationArmNca from '../../actions/Completeness/FirmArmNcaAction/filterRecouncilationArmAction';
import matchForFirmNca from '../../actions/Completeness/FirmArmNcaAction/matchForFirmAction';
import editForFirmNca from '../../actions/Completeness/FirmArmNcaAction//editForFirm';
import editForArmNca from '../../actions/Completeness/FirmArmNcaAction/editForArm';
import {Redirect} from 'react-router-dom';
const mystyle = {
    width: '325px',
    'font-size': '20px',
    border: '2px solid #dee2e6'
};
const mystyleth = {
    width: '258px'
};
const focusHeaderTh = {
    width: '258px',
    border: '0px',
    backgroundColor: '#ffffff'
}
const focusheader = {
    'font-size': '20px',
    border: '2px solid #dee2e6'
}
const bgstyle = {
    backgroundColor: '#ffdede'
};
let armData = [];
let allDataForMatch = [];
let firmData = [];
let idForFirm = [];
let firmArray = [];
let armArray = [];
let idForArm = [];
let dateArray = [];
class FilterForNca extends Component {
    constructor(props) {
        super(props);
        var importdate = new Date();
        this.state = {
            isOpen: false,
            isNotification: false,
            active: this.props.activeClass,
            showModal: false,
            showModalClass: '',
            userData: JSON.parse(sessionStorage.getItem('userData')),
            clientList: [],
            clientListNext: [],
            listFiles: [],
            listFile: 'false',
            clientType: [],
            ClientTypeName: '',
            login: true,
            to: '',
            from: '',
            loading: false,
            transpose: true,
            alltrancation: true,
            refresh: true,
            showFocusModalClass: '',
            showFocusModal: false,
            showMatchModalClass: '',
            showMatchModal: false,
            modalOpenForOther: false,
            firmIdForMatch: '',
            armIdForMatch: '',
            matchTextValue: '',
            userData: JSON.parse(sessionStorage.getItem('userData')),
            dataIdForFirm: [],
            firm: 'firm',
            dataIdForArm: [],
            arm: 'nca',
            showEditModalClass: '',
            showEditModal: false,
            selectedDay: importdate,
            selectedDayArm: importdate,
            match: true,
            redirectToFerrer:false
        };
        this.handleShowModal = this.handleShowModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.fileUpload = this.fileUpload.bind(this);
        this.handleFocusCloseModal = this.handleFocusCloseModal.bind(this);
        this.handleFocusModal = this.handleFocusModal.bind(this);
        this.generateRowsWhenTranspose = this.generateRowsWhenTranspose.bind(this);
        this.handleMatchCloseModal = this.handleMatchCloseModal.bind(this);
        this.handleMatchModal = this.handleMatchModal.bind(this);
        this.handleMatch = this.handleMatch.bind(this);
        this.tableForMatch = this.tableForMatch.bind(this);
        this.handleMatchButton = this.handleMatchButton.bind(this);
        this.handleEditCloseModal = this.handleEditCloseModal.bind(this);
        this.handleEditModal = this.handleEditModal.bind(this);
        this.handleEditData = this.handleEditData.bind(this);
        this.handleDayChange = this.handleDayChange.bind(this);
        this.handleDayChangeForArm = this.handleDayChangeForArm.bind(this);
        this.handleLoder = this.handleLoder.bind(this);
    }
    handleLoder() {
        return <div className="abs_loader"> <div className="loader"></div> </div>
    }

    handleListForFirm() {
        let idArr = [];
        idArr = this.props.inputVal;
        for (let i = 0; i < idArr.length; i++) {
            firmArray = idArr[i].split('_');
            idForFirm.push(firmArray[1]);
        }
        this.setState({ dataIdForFirm: idForFirm });
        console.log('dataid for firm', this.state.dataIdForFirm);
        let url = api.url + api.getRecouncilationDetail + '?api_token=' + this.state.userData.API_Token + '&clientName=' + this.state.userData.ClientName + '&ReconciliationType=' + this.state.firm + '&dataId=' + this.state.dataIdForFirm;
        const { dispatch } = this.props;
        dispatch(filterRecouncilationFirmNca(url));
    }
    handleListForArm() {
        let idArrForArm = [];
        idArrForArm = this.props.inputValForArm;
        for (let i = 0; i < idArrForArm.length; i++) {
            armArray = idArrForArm[i].split('_');
            idForArm.push(armArray[1]);
        }
        this.setState({ dataIdForArm: idForArm }, () => {
        });
        let url = api.url + api.getRecouncilationDetail + '?api_token=' + this.state.userData.API_Token + '&clientName=' + this.state.userData.ClientName + '&ReconciliationType=' + this.state.arm + '&dataId=' + this.state.dataIdForArm;
        const { dispatch } = this.props;
        dispatch(filterRecouncilationArmNca(url));
    }

    componentWillMount() {
        this.handleListForFirm();
        this.handleListForArm();
        this.props.matchmodal(this.state.showMatchModal);
        this.setState({ loading: false });
        let url = api.url + api.getClientType + '?api_token=' + this.state.userData.API_Token;
        // let url = api.url + api.getClient + '?api_token=' + this.state.userData.API_Token;

        // axios.get(url).then(response => {
        //     this.setState({ loading: true });
        //     this.setState({
        //         clientList: response.data.result,
        //         clientListNext: response.data.result,
        //     });
        // }).catch(error => {
        //     console.log(error);
        // });
        axios.get(url).then(response => {
            console.log(response);
            this.setState({ loading: true });
            this.setState({
                clientType: response.data.result
            });
        }).catch(error => {
            console.log(error);
        });
    }

    // getClient(field,clientId){
    //     //console.log(field);
    //     this.setState({ loading: false });
    //     if (field == 'from' || field == 'to'){
    //         let url = api.url + api.getClient + '?api_token=' + this.state.userData.API_Token +'&clientId='+clientId;
    //         axios.get(url).then(response => {
    //             if (field == 'from'){
    //                 this.setState({
    //                     clientListNext: response.data.result,
    //                 });
    //             }else if(field == 'to'){
    //                 this.setState({
    //                     clientList: response.data.result,
    //                 });
    //             }
    //             this.setState({ loading: true });
    //         }).catch(error => {
    //             console.log(error);
    //         });
    //     }
    // }

    openNavBar = () => {
        this.setState({ isOpen: !this.state.isOpen });
    }
    openNotificationBar = () => {
        this.setState({ isNotification: !this.state.isNotification });
    }

    handleCloseModal() {

        this.setState({ showModal: false, showModalClass: '' });
    }

    handleShowModal() {
        this.setState({ showModal: true, showModalClass: 'show' });
    }
    handleFocusCloseModal() {
        this.setState({ showFocusModal: false, showFocusModalClass: '', modalOpenForOther: false });
        idForFirm = [];
        this.setState({ dataIdForFirm: idForFirm }, () => {
        });
        idForArm = [];
        this.setState({ dataIdForArm: idForArm }, () => {
        });
    }
    handleFocusModal() {
        //create alert for open focus modal by krupal 20 dec
        this.handleListForFirm();
        this.handleListForArm();
        let idArr = [];
        idArr = this.props.inputVal.map(id => id);
        let idArrForArm = [];
        idArrForArm = this.props.inputValForArm.map(id => id);
        if (idArr.length === 0 && idArrForArm.length === 0) {
            alert('Select Firm or ARM transactions');
        } else {
            setTimeout(() => {
                this.setState({ showFocusModal: true, showFocusModalClass: 'show', modalOpenForOther: true });
            })
        }
    }
    handleMatchCloseModal() {
        setTimeout(() => {
            this.setState({ showMatchModal: false, showMatchModalClass: '', modalOpenForOther: false });
            armData = [];
            firmData = [];
            allDataForMatch = [];
            idForFirm = [];
            this.setState({ dataIdForFirm: idForFirm }, () => {
            });
            idForArm = [];
            this.setState({ dataIdForArm: idForArm }, () => {
            });
            this.setState({ handleTextForMatch: '' }, () => {
            });

        }, 1050);
    }
    handleMatchModal(e) {
        this.handleListForFirm();
        this.handleListForArm();
        const target = e.target;
        this.setState({ match: target.value });
        this.props.onMatchClick(this.state.match);
        if (this.state.showMatchModal === true) {
            this.setState({ match: false });
            this.props.onMatchClick(this.state.match);
        } else {
            this.setState({ match: true });
            this.props.onMatchClick(this.state.match);
        }
        let idArr = [];
        idArr = this.props.inputVal.map(id => id);
        let idArrForArm = [];
        idArrForArm = this.props.inputValForArm.map(id => id);
        if (idArr.length === 0 && idArrForArm.length === 0) {
            alert('Select Firm and ARM transactions');
        } else if (idArr.length !== idArrForArm.length) {
            alert('Must Select Atleast Single Data From Both Table');
        } else {
            setTimeout(() => {
                let referenceForFirm;
                let referenceForArm;
                referenceForFirm = this.props.recouncilationdata[0].TRANSACTION_REFERENCE_NUMBER;
                referenceForArm = this.props.armrecouncilationdata[0].TRANSACTION_REFERENCE_NUMBER;
                console.log('reference for firm', referenceForFirm);
                console.log('reference for arm', referenceForArm);
                console.log('id for firm', idArr);
                console.log('id for arm', idArrForArm);
                if (idArr.length === 0 && idArrForArm.length === 0) {
                    alert('Select Firm and ARM transactions');
                } else if (idArr.length > 1 || idArrForArm.length > 1) {
                    alert('Select Single Firm and ARM transactions')
                }
                else if (idArr.length !== idArrForArm.length) {
                    alert('Must Select Atleast Single Data From Both Table');
                }
                else if (referenceForFirm !== referenceForArm) {
                    idForFirm = [];
                    this.setState({ dataIdForFirm: idForFirm }, () => {
                        console.log(this.state.dataIdForFirm);
                    });
                    idForArm = [];
                    this.setState({ dataIdForArm: idForArm }, () => {
                        console.log(this.state.dataIdForArm);
                    });
                    alert('Transaction Reference Number Must Be Same For Match...');
                }
                else {
                    setTimeout(() => {
                        this.setState({ showMatchModal: true, showMatchModalClass: 'show', modalOpenForOther: true });
                        console.log('filter match', this.state.showMatchModal);
                        this.props.matchmodal(this.state.showMatchModal);
                    }, 1000);
                }
            }, 2000);
        }
    }

    handleEditCloseModal() {
        this.setState({ showEditModal: false, showEditModalClass: '', modalOpenForOther: false });
        dateArray = [];
    }
    handleEditModal() {
        this.handleListForFirm();
        this.handleListForArm();
        let idArr = [];
        idArr = this.props.inputVal.map(id => id);
        let idArrForArm = [];
        idArrForArm = this.props.inputValForArm.map(id => id);
        if (idArrForArm.length === 0 && idArr.length === 0) {
            alert('Select Firm or ARM transactions')
        }
        else if (idArr.length === 1 && idArrForArm.length === 0 || idArr.length === 0 && idArrForArm.length === 1) {
            this.setState({ showEditModal: true, showEditModalClass: 'show', modalOpenForOther: true });
        } else {
            alert('Only One Data To Be Selected...');
        }
    }
    handleUserInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        var alldata = this.state;
        alldata[name] = value
        //this.getClient(e.target.name, e.target.value)
        this.setState(alldata);
    }
    onFormSubmit(e) {
        e.preventDefault() // Stop form submit
        console.log('client type',this.state.ClientTypeName)
        if(this.state.ClientTypeName === ''){
            alert('Must select client type');
        }else{
        if (this.state.file) {
            this.fileUpload(this.state.file);
        } else {
            alert('File must be in .csv formate');
        }
    }
    }
    onChange(e) {
        if (e.target.files[0].type === "application/vnd.ms-excel" || e.target.files[0].type === "text/csv") {
            this.setState({ file: e.target.files[0], isDisable: false })
            console.log(e.target.files[0]);

        } else {
            alert('File must be in .csv formate');
        }
    }
    fileUpload(file) {
        var formData = new FormData();
        formData.append("file", file);
        formData.append("api_token", this.state.userData.API_Token);
        formData.append("ClientName", JSON.parse(sessionStorage.getItem('userData')).ClientName);
        formData.append("clientType", this.state.ClientTypeName);
        let url = api.url + api.uploadFiles
        
        axios.post(url, formData, {
            headers: { 'Content-Type': 'multipart/form-data;text/html' }
        }).then(response => {
            if(response.status === 200){
                if (response.data.status) {
                    console.log('status',response);
                    alert('File Uploaded')
                    this.handleCloseModal();
                } else {
                    alert(response.data.message)
                
                }
            }else{
                alert('Please Login First')
                sessionStorage.removeItem('userData');
                this.setState({redirectToFerrer:true});
            }
                }).catch(error => {


        });
    }
    handleRefresh(e) {
        const target = e.target;
        this.setState({ refresh: target.value });
        this.props.onRefreshClick(this.state.refresh);
        if (this.state.refresh === true) {
            this.setState({ refresh: false });
            this.props.onRefreshClick(this.state.refresh);
        } else {
            this.setState({ refresh: true });
            this.props.onRefreshClick(this.state.refresh);
        }
    }
    handleTrans(e) {
        const target = e.target;
        this.setState({ transpose: target.value });
        this.props.ontransClick(this.state.transpose);
        if (this.state.transpose === true) {
            this.setState({ transpose: false });
            this.props.ontransClick(this.state.transpose);
        } else {
            this.setState({ transpose: true });
            this.props.ontransClick(this.state.transpose);
        }
    }
    handleAllTransaction(e) {
        const target = e.target;
        this.setState({ alltrancation: target.value });
        this.props.onTransactionclick(this.state.alltrancation);
        if (this.state.alltrancation === true) {
            this.setState({ alltrancation: false });
            this.props.onTransactionclick(this.state.alltrancation);
        } else {
            this.setState({ alltrancation: true });
            this.props.onTransactionclick(this.state.alltrancation);
        }
    }
    getanotherColumnTransposeForArm(column, status) {
        if (this.state.modalOpenForOther) {
            let columnName = column;
            let columns = [];
            var columnsRecouncilationmap = this.props.armrecouncilationdata;
            let idArrForArm = [];
            idArrForArm = this.props.inputValForArm.map(id => id);
            if (status) {
                for (let j = 0; j < idArrForArm.length; j++) {
                    for (let i = 0; i < columnsRecouncilationmap.length; i++) {

                        if (idArrForArm[j] === `column_${idForArm[i]}`) {
                            columns.push(
                                <td key={`col_${idForArm[i]}`}>{columnsRecouncilationmap[i][columnName]}</td>
                            )
                            break;
                        }
                    }
                }

            }
            return columns;
        }
    }
    generateRowsWhenTransposeForArm() {
        if (this.state.modalOpenForOther) {
            if (!(this.props.armrecouncilationColumns.length === 0 && this.props.armrecouncilationdata.length === 0)) {
                let columns = this.props.armrecouncilationColumns;
                if (!(columns.length === 0)) {
                    const rows = [];
                    rows.push(
                        <tr key={`row_`}>
                            {this.getanotherColumnTransposeForArm(0, false)}
                        </tr>
                    )
                    for (let i = 0; i < columns.length; i++) {
                        const value = columns[i].split('_').join(' ');
                        const ckd = value.toString();
                        if (!(ckd === 'BreakStatus')) {
                            rows.push(
                                <tr key={ckd} >
                                    <th>{value}</th>
                                    {this.getanotherColumnTransposeForArm(columns[i], true)}
                                </tr>

                            );
                        }
                    }

                    return rows;
                }
            }
        }
    }
    getanotherColumnTranspose(column, status) {
        if (this.state.modalOpenForOther) {
            let columnName = column;
            let columns = [];
            var columnsRecouncilationmap = this.props.recouncilationdata;
            let idArr = [];
            idArr = this.props.inputVal.map(id => id);
            if (status) {
                for (let j = 0; j < idArr.length; j++) {

                    for (let i = 0; i < columnsRecouncilationmap.length; i++) {
                        if (idArr[j] === `column_${idForFirm[i]}`) {
                            columns.push(
                                <td key={`col_${idForFirm[i]}`}>{columnsRecouncilationmap[i][columnName]}</td>
                            )
                        }
                    }
                }
            }
            return columns;
        }
    }

    generateRowsWhenTranspose() {
        if (this.state.modalOpenForOther) {
            if (!(this.props.recouncilationColumns.length === 0 && this.props.recouncilationdata.length === 0 && this.props.armrecouncilationdata.length === 0)) {
                let columns;
                if (this.props.inputVal.length >= 1) {
                    columns = this.props.recouncilationColumns
                }
                if (this.props.inputValForArm.length >= 1) {
                    columns = this.props.armrecouncilationColumns
                }
                if (!(columns.length === 0)) {
                    const rows = [];
                    for (let i = 0; i < columns.length; i++) {
                        const value = columns[i].split('_').join(' ');
                        const ckd = value.toString();
                        if (!(ckd === 'BreakStatus')) {
                            rows.push(
                                <tr key={ckd} >
                                    <th>{value}</th>
                                    {this.getanotherColumnTranspose(columns[i], true)}
                                    {this.getanotherColumnTransposeForArm(columns[i], true)}
                                </tr>

                            );
                        }
                    }
                    return rows;
                }
            }
        }
    }
    getanotherColumnTransposeForArmForMatch(column, status) {
        if (this.state.modalOpenForOther) {
            let columnName = column;
            let columns = [];
            var columnsRecouncilationmap = this.props.armrecouncilationdata;
            let idArrForArm = [];
            idArrForArm = this.props.inputValForArm.map(id => id);
            if (status) {
                for (let j = 0; j < idArrForArm.length; j++) {

                    for (let i = 0; i < columnsRecouncilationmap.length; i++) {
                        if (idArrForArm[j] === `column_${idForArm[i]}`) {
                            armData.push(columnsRecouncilationmap[i][columnName]);
                            var resArm = armData.filter(function (n) { return !this.has(n) },
                                new Set(firmData));
                            columns.push(
                                <td key={`col_${idForArm[i]}`}>{columnsRecouncilationmap[i][columnName]}</td>
                            )
                            break;
                        }
                    }
                }
                // armData = Array.from(new Set(armData));
            }
            return columns;
        }
    }

    getanotherColumnMatch(column, status) {
        if (this.state.modalOpenForOther) {
            let columnName = column;
            let columns = [];
            var columnsRecouncilationmap = this.props.recouncilationdata;
            let idArr = [];
            idArr = this.props.inputVal.map(id => id);
            if (status) {
                for (let j = 0; j < idArr.length; j++) {

                    for (let i = 0; i < columnsRecouncilationmap.length; i++) {

                        if (idArr[j] === `column_${idForFirm[i]}`) {
                            firmData.push(columnsRecouncilationmap[i][columnName]);
                            var resFirm = firmData.filter(function (n) { return !this.has(n) },
                                new Set(armData));
                            columns.push(
                                <td key={`col_${idForFirm[i]}`}>{columnsRecouncilationmap[i][columnName]}</td>
                            )
                            break;
                        }
                    }
                }
            }
            return columns;
        }

    }

    tableForMatch() {
        if (this.state.modalOpenForOther) {
            if (!(this.props.recouncilationColumns.length === 0 && this.props.recouncilationdata.length === 0)) {
                let columns = this.props.recouncilationColumns;
                if (!(columns.length === 0)) {
                    const rows = [];
                    rows.push(
                        <tr key={`row_`}>
                        </tr>
                    )
                    for (let i = 0; i < columns.length; i++) {
                        const value = columns[i].split('_').join(' ');
                        const ckd = value.toString();
                        let differ = '';
                        if (this.props.recouncilationdata[0][columns[i]] === this.props.armrecouncilationdata[0][columns[i]]) {
                            differ = '';
                        } else {
                            differ = 'differ';
                        }
                        rows.push(
                            <tr key={`row_${i}`} className={differ} >
                                <th>{value}</th>
                                <td>{this.props.recouncilationdata[0][columns[i]]}</td>
                                <td>{this.props.armrecouncilationdata[0][columns[i]]}</td>
                            </tr>
                        );

                    }
                    return rows;
                }
            }
        }
    }
    handleMatchButton(e) {
        e.preventDefault();
        let url = api.url + api.matchRecouncilationDetail +
        '?api_token=' + this.state.userData.API_Token +
        '&import_date=' + this.props.recouncilationdata[0].IMPORT_DATE +
        '&clientName=' + this.state.userData.ClientName +
        '&Reconciliation_type=Firm-Arm' +
        '&transaction_refrence=' + this.props.recouncilationdata[0].TRANSACTION_REFERENCE_NUMBER +
        '&transaction_status=' + this.props.recouncilationdata[0].REPORT_STATUS +
        '&reason=' + this.state.matchTextValue;
    const { dispatch } = this.props;
    dispatch(matchForFirmNca(url));
        if (this.props.getMatch === true) {
            alert('Match Updated Successfully');
            this.handleMatchCloseModal();
        }
    }
    handleTextForMatch(e) {
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value
        });
        
    }
    handleMatch() {
        if (this.state.modalOpenForOther) {
            let idArr = [];
            idArr = this.props.inputVal.map(id => id);
            let idArrForArm = [];
            idArrForArm = this.props.inputValForArm.map(id => id);
            if (idArr.length !== idArrForArm.length) {
                return <p>Must Select From Both Table</p>
            }
            if (idArr.length > 1 || idArrForArm.length > 1) {
                return <p>Must Select Only One Data From Both Table</p>
            } else {
                return <div>
                    <br />

                    <br />
                    <table>
                        <tbody>
                            <tr>
                                <th style={mystyleth}></th>
                                {idArr.length > 0 ? <td style={mystyle}>Firm-Arm Transaction</td> : null}
                                {idArrForArm.length > 0 ? <td style={mystyle}>Arm-Firm Transaction</td> : null}
                            </tr>
                        </tbody>
                    </table>

                    <div className="table-responsive">
                        <table className="fixed_header table table-striped table-bordered table-hover">
                            <tbody>
                                {this.tableForMatch()}
                            </tbody>
                        </table>
                    </div>
                    <h4>Reason For Match:</h4>
                    <textarea className="form-control" rows="5" value={this.state.matchTextValue} name="matchTextValue"
                        onChange={this.handleTextForMatch.bind(this)} />
                    <br />
                    <button className="btn btn-primary btn-lg float-right" onClick={this.handleMatchButton}>Match</button>
                </div>

            }
        }
    }
    //focus
    handleHeaderForFocus() {
        if (this.state.modalOpenForOther) {
            let idArr = [];
            idArr = this.props.inputVal.map(id => id);
            const rows = [];
            if (idArr.length > 0) {
                for (let i = 0; i < idArr.length; i++) {
                    return <td style={focusheader} colSpan={idArr.length}>Firm-Arm Transactions</td>
                }
            }
        }
    }
    handleHeaderForFocusForArm() {
        if (this.state.modalOpenForOther) {
            let idArrForArm = [];
            idArrForArm = this.props.inputValForArm.map(id => id);
            if (idArrForArm.length > 0) {
                for (let i = 0; i < idArrForArm.length; i++) {
                    return <td style={focusheader} colSpan={idArrForArm.length}>Arm-Firm Transactions</td>
                }

            }
        }
    }
    FocusModal() {
        if (this.state.modalOpenForOther) {
            let idArr = [];
            idArr = this.props.inputVal.map(id => id);
            let idArrForArm = [];
            idArrForArm = this.props.inputValForArm.map(id => id);
            return <div>
                <div className="table-responsive">
                    <table className="fixed_header editscroll  table table-striped table-bordered table-hover">
                        <tbody>
                            <tr>
                                <td style={focusHeaderTh}></td>
                                {this.handleHeaderForFocus()}
                                {this.handleHeaderForFocusForArm()}

                            </tr>
                            {this.generateRowsWhenTranspose()}
                        </tbody>
                    </table>
                </div>
            </div>

        }
    }
    //Edit Metheds..
    handleEditData() {
        let idArr = [];
        idArr = this.props.inputVal.map(id => id);
        let idArrForArm = [];
        idArrForArm = this.props.inputValForArm.map(id => id);
        if (idArr.length > 0) {
            return <div className="table-responsive">
                <table className="fixed_header editscroll table table-striped table-bordered table-hover">
                    <tbody>
                        {this.generateRowsForEdit()}
                    </tbody>
                </table>
                <button className="btn btn-primary btn-lg float-right" onClick={this.handleSubmitEdit.bind(this)}>Submit</button>
            </div>
        } else {
            return <div className="table-responsive">
                <table className="fixed_header editscroll table table-striped table-bordered table-hover">
                    <tbody>
                        {this.generateRowsForEditForArm()}
                    </tbody>
                </table>
                <button className="btn btn-primary btn-lg float-right" onClick={this.handleSubmitEditForArm.bind(this)}>Submit</button>
            </div>
        }
    }
    handleDayChange(day, value, dayPickerInput) {
        const input = dayPickerInput.getInput();
        console.log('input', input.placeholder);
        console.log('input', input.value);
        let date = this.props.recouncilationdata;
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        day = day.getDate() + '-' + monthNames[day.getMonth()] + '-' + day.getFullYear();
        if (input.placeholder === 'IMPORT_DATE') {
            date[0].IMPORT_DATE = day;
            date = date;
            this.setState({ recouncilationdata: date }, () => {
                console.log(this.props.recouncilationdata);
            })
        } else if (input.placeholder === 'MATURITY_DATE') {
            date[0].MATURITY_DATE = day;
            date = date;
            this.setState({ recouncilationdata: date }, () => {
                console.log(this.props.recouncilationdata);
            })
        } else if (input.placeholder === 'EXPIRY_DATE') {
            date[0].EXPIRY_DATE = day;
            date = date;
            this.setState({ recouncilationdata: date }, () => {
                console.log(this.props.recouncilationdata);
            })
        }
    }
    generateRowsForEdit() {
        if (this.state.modalOpenForOther) {
            if (!(this.props.recouncilationColumns.length === 0 && this.props.recouncilationdata.length === 0)) {
                let columns = this.props.recouncilationColumns;
                if (!(columns.length === 0)) {
                    const rows = [];
                    rows.push(
                        <tr key={`row_`}>

                        </tr>
                    )
                    for (let i = 0; i < columns.length; i++) {
                        const columneValue = columns[i]
                        const value = columns[i].split('_').join(' ');
                        const ckd = value.toString();
                        const { selectedDay } = this.state.selectedDay;
                        if (columns[i] === 'IMPORT_DATE' || columns[i] === 'MATURITY_DATE' || columns[i] === 'EXPIRY_DATE') {
                            rows.push(
                                <tr key={ckd}>
                                    <th>{value}</th>
                                    <td>
                                        <DayPickerInput
                                            onDayChange={this.handleDayChange}
                                            className="date-input"
                                            format="LL"
                                            formatDate={formatDate}
                                            parseDate={parseDate}
                                            value={this.props.recouncilationdata[0][columns[i]]}
                                            placeholder={[columns[i]]}
                                        />
                                    </td>
                                </tr>
                            );
                        } else {
                            rows.push(
                                <tr key={ckd}>
                                    <th>{value}</th>
                                    {/* {this.getanotherColumnMatch(columns[i], true)} */}
                                    {/* {this.getanotherColumnTransposeForArmForMatch(columns[i], true)} */}
                                    <td><input name={columneValue} value={this.props.recouncilationdata[0][columns[i]]} onChange={this.handleTextBoxForEdit.bind(this)} /></td>
                                </tr>
                            );
                        }
                    }
                    console.log('Import Date---->', this.props.recouncilationdata[0].IMPORT_DATE);
                    console.log('Firm-Arm');
                    console.log('Transaction Reference Number---->',
                        this.props.recouncilationdata[0].TRANSACTION_REFERENCE_NUMBER);
                    return rows;
                }
            }

        }
    }
    handleTextBoxForEdit(e) {
        let update = this.props.recouncilationdata;
        console.log(e.target.name);
        update[0][e.target.name] = e.target.value;
        update = update;
        this.setState({
            ...this.state,
            recouncilationdata: update
        }, () => {
        });


    }
    handleSubmitEdit(e) {
        e.preventDefault();
        let user = {
            'api_token': this.state.userData.API_Token,
            'dataId=': this.state.dataIdForFirm,
            'data': JSON.stringify(this.props.recouncilationdata[0]),
            'clientName': this.state.userData.ClientName,
            'recouncilationType': 'Firm-Arm'
        }
        const { dispatch } = this.props;
        dispatch(editForFirmNca(user));
        this.setState({ showEditModal: false });
    }
    handleDayChangeForArm(day, value, dayPickerInput) {
        const input = dayPickerInput.getInput();
        console.log('input', input.placeholder);
        console.log('input', input.value);
        let date = this.props.armrecouncilationdata;
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        day = day.getDate() + '-' + monthNames[day.getMonth()] + '-' + day.getFullYear();
        if (input.placeholder === 'IMPORT_DATE') {
            date[0].IMPORT_DATE = day;
            date = date;
            this.setState({ armrecouncilationdata: date }, () => {
                console.log(this.props.armrecouncilationdata);
            })
        } else if (input.placeholder === 'MATURITY_DATE') {
            date[0].MATURITY_DATE = day;
            date = date;
            this.setState({ armrecouncilationdata: date }, () => {
                console.log(this.props.armrecouncilationdata);
            })
        } else if (input.placeholder === 'EXPIRY_DATE') {
            date[0].EXPIRY_DATE = day;
            date = date;
            this.setState({ armrecouncilationdata: date }, () => {
                console.log(this.props.armrecouncilationdata);
            })
        }
    }
    generateRowsForEditForArm() {
        if (this.state.modalOpenForOther) {
            if (!(this.props.armrecouncilationColumns.length === 0 && this.props.armrecouncilationdata.length === 0)) {
                let columns = this.props.armrecouncilationColumns;
                if (!(columns.length === 0)) {
                    const rows = [];
                    rows.push(
                        <tr key={`row_`}>

                        </tr>

                    )
                    for (let i = 0; i < columns.length; i++) {
                        const columneValue = columns[i]
                        const value = columns[i].split('_').join(' ');
                        const ckd = value.toString();
                        const { selectedDayArm } = this.state.selectedDayArm;
                        if (columns[i] === 'IMPORT_DATE' || columns[i] === 'MATURITY_DATE' || columns[i] === 'EXPIRY_DATE') {
                            rows.push(
                                <tr key={ckd}>
                                    <th>{value}</th>
                                    {/* {this.getanotherColumnMatch(columns[i], true)} */}
                                    {/* {this.getanotherColumnTransposeForArmForMatch(columns[i], true)} */}
                                    <td>

                                        <DayPickerInput
                                            onDayChange={this.handleDayChangeForArm}
                                            className="date-input"
                                            format="LL"
                                            formatDate={formatDate}
                                            parseDate={parseDate}
                                            value={this.props.armrecouncilationdata[0][columns[i]]}
                                            placeholder={[columns[i]]}
                                        />

                                    </td>
                                </tr>
                            );
                        } else {
                            rows.push(
                                <tr key={ckd}>
                                    <th>{value}</th>
                                    {/* {this.getanotherColumnMatch(columns[i], true)} */}
                                    {/* {this.getanotherColumnTransposeForArmForMatch(columns[i], true)} */}
                                    <td><input name={columneValue} value={this.props.armrecouncilationdata[0][columns[i]]} onChange={this.handleTextBoxForEditForArm.bind(this)} /></td>
                                </tr>
                            );
                        }
                    }

                    return rows;
                }
            }
        }
    }
    handleTextBoxForEditForArm(e) {
        let update = this.props.armrecouncilationdata;
        console.log(e.target.name);
        console.log(update[0][e.target.name] = e.target.value);
        update = update;
        this.setState({
            ...this.state,
            armrecouncilationdata: update
        }, () => {
            console.log(this.props.armrecouncilationdata);
        });
    }
    handleSubmitEditForArm(e) {
        e.preventDefault();
        let user = {
            '?api_token=': this.state.userData.API_Token,
            '&dataId=': this.state.dataIdForFirm,
            '&data=': this.props.armrecouncilationdata[0],
            '&clientName=': this.state.userData.ClientName,
            '&recouncilationType=': 'Firm-Arm'
        }
        const { dispatch } = this.props;
        dispatch(editForArmNca(user));
    }
    render() {
        if(this.state.redirectToFerrer === true){
            return <Redirect exact push to={{ pathname: '/' }} />
        }
        return (

            <div className="col-xl-6 col-sm-6 pull-right">
                <div className="filter-search-div">
                    <div className="filter-nav-div ">
                        <div className="filt-nav-ul clearfix">
                            <ul>
                                <li>

                                    {/* <input type="file" id="file1" name="image" accept="image/*" capture style={{"display":"none"}}/> */}
                                    <span onClick={this.handleShowModal} className="ion-upload arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Upload" title="Upload"></span>

                                </li>
                                <li>

                                    <label onClick={this.handleRefresh.bind(this)}
                                        value={this.props.refresh} className="ion-refresh arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Refresh" title="Refresh"></label>

                                </li>
                                {/* <!--li><a href="#"><span className="ion-search" data-toggle="tooltip" data-placement="top" data-title="Search" title="Search"></span></a></li--> */}
                                {/* <li><a href="#"><span className="ion-android-sort" data-toggle="tooltip" data-placement="top" data-title="Sort" title="Sort"></span></a></li> */}
                                <li>

                                    <label onClick={this.handleTrans.bind(this)}
                                        value={this.state.transpose} className="ion-android-arrow-down-right arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Transpose" title="Transpose"></label>

                                </li>
                                <li>
                                    <span onClick={this.handleFocusModal} className="ion-flash arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Focus" title="Focus">
                                    </span>

                                </li>
                                <li>

                                    <span onClick={this.handleMatchModal} value={this.state.match} className="ion-magnet arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Match" title="Match"></span>

                                </li>
                                <li>

                                    <label onClick={this.handleAllTransaction.bind(this)}
                                        value={this.state.alltrancation} className="ion-social-buffer arrowpadding" data-toggle="tooltip" data-placement="top" data-title="All Transaction" title="All Transaction"></label>

                                </li>
                                <li>
                                    <span onClick={this.handleEditModal}
                                        className="ion-compose arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Edit" title="Edit"></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {/* <!-- end of filter-nav-div--> */}
                    <div className="search-data-div">
                        <div className="clearfix">
                            <form>
                                <div className="search-col-1"><input type="text" className="form-control" placeholder="Search" /></div>
                                <div className="search-col-6"><button className="btn sbtn">Search</button></div>
                            </form>
                        </div>
                    </div>
                </div>
                {/* <!-- end of filter-search-div --> */}
                <Modal className={`${this.state.showModalClass}`} show={this.state.showModal} onHide={this.handleCloseModal}>
                    <Modal.Header className="modal-header1" closeButton>
                        <Modal.Title className="title-files">Upload Files</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="container">
                            <div className="demoForm">
                                {!this.state.loading ? <div><i className="fa fa-spin fa-fw fa-spinner"></i> Loding</div> :
                                    <form onSubmit={this.onFormSubmit}>
                                        {/* <div className={`form-group col-md-12 radio-root1 clearfix`}>
                                            <label className="col-md-12 padd-0 clearfix " htmlFor="email">Recouncilation Type</label>
                                            
                                            <div className="radio-inline clearfix">
                                                <span for="Firm-ARM" className="radio-span1">Firm-ARM </span> 
                                                <input type="radio" id="Firm-ARM" onChange={(event) => this.handleUserInput(event)} name="recouncilationType" className=" form-control radio-input" value="Firm-ARM" />
                                            </div>
                                            <div className="radio-inline clearfix">
                                                <span className="radio-span1" for="Firm-ARM-FCA">Firm-ARM-FCA </span>
                                                <input type="radio" id="Firm-ARM-FCA" onChange={(event) => this.handleUserInput(event)} name="recouncilationType" className=" form-control radio-input" value="Firm-ARM-FCA" />
                                            </div>

                                        </div> */}
                                        <div className="form-group clearfix">
                                            <label htmlFor="state" className="col-md-12 control-label clearfix">Client Type </label>
                                            <div className="col-md-12 clearfix">
                                                {/* {ClientTypes} */}
                                                {
                                                    this.state.clientType.map((option, i) => {
                                                        return <label key={i}>
                                                            <input
                                                                type="radio"
                                                                name="ClientTypeName"
                                                                key={i}
                                                                onChange={this.handleUserInput.bind(this)}
                                                                value={option.ClientTypeName} />
                                                            {option.ClientTypeName}
                                                        </label>
                                                    })
                                                }
                                            </div>

                                        </div>
                                        <div className={`form-group col-md-12 clearfix`}>
                                            <label className="col-md-12 padd-0 clearfix" htmlFor="email">File Upload</label>
                                            <input type="file" onChange={this.onChange} className=" form-control input-file" />
                                            <label>Saved your file as fromClient_toClient_Date.csv</label><br />
                                            <label>Example:-Schroders_UnaVista_31-01-2019.csv</label><br />
                                            <label>File type must be in csv formate</label>
                                        </div>
                                        {/* <div className="form-group">
                                            <label htmlFor="state" className="col-md-2 control-label">From </label>
                                            <div className="col-md-6 ">
                                                <select className="form-control" defaultValue='' onChange={(event) => this.handleUserInput(event)} required="" id="from" name="from">
                                                    <option value=''>Select</option>
                                                    {allProfiles}
                                                </select>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="state" className="col-md-2 control-label">To </label>
                                            <div className="col-md-6">
                                                <select className="form-control" defaultValue='' onChange={(event) => this.handleUserInput(event)} required="" id="to" name="to">
                                                    <option value=''>Select</option>
                                                    {allClients}
                                                </select>
                                            </div>
                                        </div> */}
                                        <div className="col-md-9">
                                            <button type="submit" className="btn btn-primary" disabled={this.state.isDisable} >Upload</button>
                                        </div>
                                    </form>
                                }
                                {/* <div className="cols col-md-3">
                        <a href="/forgot-password" className="btn btn-primary">Forgot Password</a>
                    </div> */}

                            </div>



                        </div>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleCloseModal}>Close</Button>
                    </Modal.Footer>
                </Modal>
                {/* focus model */}
                <Modal className={`${this.state.showFocusModalClass}`} show={this.state.showFocusModal} onHide={this.handleFocusCloseModal}>
                    <Modal.Header className="modal-header1" closeButton>
                        <Modal.Title className="title-files">Focus On Firm-Arm And Arm-Firm Transactions</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="modal-new1">
                            <div className="row">
                                <div className="col-xl-12">
                                    <div className="card m-b-20">

                                        <div className="card-body">
                                            {this.FocusModal()}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleFocusCloseModal}>Close</Button>
                    </Modal.Footer>
                </Modal>
                {/* match modal */}

                <Modal className={`${this.state.showMatchModalClass}`} show={this.state.showMatchModal} onHide={this.handleMatchCloseModal}>
                    <Modal.Header className="modal-header1" closeButton>
                        <Modal.Title className="title-files">Match On Firm-Arm And Arm-Firm Transaction</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="modal-new1">
                            <div className="row">
                                <div className="col-xl-12">
                                    <div className="card m-b-20">

                                        <div className="card-body">
                                            {this.handleMatch()}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleMatchCloseModal}>Close</Button>
                    </Modal.Footer>
                </Modal>

                {/* edit modal */}
                <Modal className={`${this.state.showEditModalClass}`} show={this.state.showEditModal} onHide={this.handleEditCloseModal}>
                    <Modal.Header className="modal-header1" closeButton>
                        <Modal.Title className="title-files">Edit On Firm-Arm Transactions</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="modal-new1">
                            <div className="row">
                                <div className="col-xl-12">
                                    <div className="card m-b-20">
                                        <div className="card-body">
                                            {this.handleEditData()}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleEditCloseModal}>Close</Button>
                    </Modal.Footer>
                </Modal>

            </div>

        )
    }
};
function mapStateToProps(state) {
    const { recouncilationdata } = state.filterRecouncilationFirm
    const { recouncilationColumns } = state.filterRecouncilationFirm
    const { armrecouncilationdata } = state.filterRecouncilationArm
    const { armrecouncilationColumns } = state.filterRecouncilationArm
    const { getMatch } = state.matchForFirm
    const { dataEditFirm } = state.editForFirm
    const { dataEditARM } = state.editForArm

    return {
        recouncilationdata,
        recouncilationColumns,
        armrecouncilationdata,
        armrecouncilationColumns,
        getMatch,
        dataEditFirm,
        dataEditARM
    };
}

export default connect(mapStateToProps)(FilterForNca);

