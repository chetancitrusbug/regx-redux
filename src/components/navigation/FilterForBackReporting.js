import React, { Component } from 'react';
class FilterForBackReporting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            transpose: true,
            alltrancation: true,
            refresh: true,
        }
    }
    handleTrans(e) {
        const target = e.target;
        this.setState({ transpose: target.value });
        this.props.ontransClick(this.state.transpose);
        if (this.state.transpose === true) {
            this.setState({ transpose: false });
            this.props.ontransClick(this.state.transpose);
        } else {
            this.setState({ transpose: true });
            this.props.ontransClick(this.state.transpose);
        }
    }
    handleAllTransaction(e) {
        const target = e.target;
        this.setState({ alltrancation: target.value });
        this.props.onTransactionclick(this.state.alltrancation);
        if (this.state.alltrancation === true) {
            this.setState({ alltrancation: false });
            this.props.onTransactionclick(this.state.alltrancation);
        } else {
            this.setState({ alltrancation: true });
            this.props.onTransactionclick(this.state.alltrancation);
        }
    }
    handleRefresh(e) {
        const target = e.target;
        this.setState({ refresh: target.value });
        this.props.onRefreshClick(this.state.refresh);
        if (this.state.refresh === true) {
            this.setState({ refresh: false });
            this.props.onRefreshClick(this.state.refresh);
        } else {
            this.setState({ refresh: true });
            this.props.onRefreshClick(this.state.refresh);
        }
    }
    render() {
        return (

            <div className="col-xl-6 col-sm-6 pull-right">
                <div className="filter-search-div">
                    <div className="filter-nav-div ">
                        <div className="filt-nav-ul clearfix">
                            <ul>
                                {/* <li>


                                    <span  className="ion-upload arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Upload" title="Upload"></span>

                                </li> */}
                                <li>

                                    <label onClick={this.handleRefresh.bind(this)}
                                        value={this.props.refresh} className="ion-refresh arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Refresh" title="Refresh"></label>

                                </li>
                                {/* <li>

                                    <label  className="ion-refresh arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Refresh" title="Refresh"></label>

                                </li> */}
                                {/* <!--li><a href="#"><span className="ion-search" data-toggle="tooltip" data-placement="top" data-title="Search" title="Search"></span></a></li--> */}
                                {/* <li><a href="#"><span className="ion-android-sort" data-toggle="tooltip" data-placement="top" data-title="Sort" title="Sort"></span></a></li> */}
                                <li>

                                    <label onClick={this.handleTrans.bind(this)}
                                        value={this.state.transpose} className="ion-android-arrow-down-right arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Transpose" title="Transpose"></label>

                                </li>
                                <li>

                                    <label onClick={this.handleAllTransaction.bind(this)}
                                        value={this.state.alltrancation} className="ion-social-buffer arrowpadding" data-toggle="tooltip" data-placement="top" data-title="All Transaction" title="All Transaction"></label>

                                </li>
                                {/* <li>
                                    <span  className="ion-flash arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Focus" title="Focus">
                                    </span>

                                </li>
                                <li>
                                    
                                    <span className="ion-magnet arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Match" title="Match"></span>
                                    
                                </li>
                                <li>

                                    <label  className="ion-social-buffer arrowpadding" data-toggle="tooltip" data-placement="top" data-title="All Transaction" title="All Transaction"></label>

                                </li>
                                <li>
                                    <span 
                                   className="ion-compose arrowpadding" data-toggle="tooltip" data-placement="top" data-title="Edit" title="Edit"></span>
                                </li> */}
                            </ul>
                        </div>
                    </div>
                    {/* <!-- end of filter-nav-div--> */}
                    <div className="search-data-div">
                        <div className="clearfix">
                            <form>
                                <div className="search-col-1"><input type="text" className="form-control" placeholder="Search" /></div>
                                <div className="search-col-6"><button className="btn sbtn">Search</button></div>
                            </form>
                        </div>
                    </div>
                </div>
                {/* <!-- end of filter-search-div --> */}

            </div>

        );
    }
}

export default FilterForBackReporting;