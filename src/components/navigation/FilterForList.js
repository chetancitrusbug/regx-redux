import React, { Component } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { connect } from 'react-redux';
import api from '../../helpers/api';
import axios from 'axios';
import listDataAction from '../../actions/List/listdataAction';
let idForFirm = [];
let firmArray = [];

class FilterForList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showEditModalClass: '',
            showEditModal: false,
            modalOpenForOther: false,
            userData: JSON.parse(sessionStorage.getItem('userData')),
            client:[],
            columns:[],
            dataIdForFirm:[],
            delete:false
        }
        this.handleEditCloseModal = this.handleEditCloseModal.bind(this);
        this.handleEditModal = this.handleEditModal.bind(this);
        this.handleDeleteData = this.handleDeleteData.bind(this);
    }
    handleListForFirm(){
        let idArr = [];
        idArr = this.props.inputVal;
        for(let i=0;i<idArr.length;i++){
            firmArray = idArr[i].split('_');
            console.log(firmArray[1]);
        }
        idForFirm = firmArray[1];
        this.setState({dataIdForFirm:idForFirm});
       let url = api.url + api.userDetials +  '?api_token=' + this.state.userData.API_Token + '&user_id=' + this.state.userData.USERID + '&get_data_for_user=' +  idForFirm; 
       axios.get(url).then(response => {
      
        if (response.data.status) {
            this.setState({client:response.data.result.client,columns:response.data.result.columns})
        } else {
            this.setState({client:'',columns:''})
        }  
    }).catch(error => {
        console.log(error);
    });

       // const {dispatch} = this.props;
        // dispatch(filterRecouncilationFirm(url));

    }

    handleEditCloseModal() {
        this.setState({ showEditModal: false, showEditModalClass: '', modalOpenForOther: false });
    }
    handleEditModal() {
        this.handleListForFirm();
        let idArr = [];
        idArr = this.props.inputVal.map(id => id);
        if (idArr.length === 0) {
            alert('Select Data')
        } else if (idArr.length === 1) {
            this.setState({ showEditModal: true, showEditModalClass: 'show', modalOpenForOther: true });
        } else {
            alert('Only One Data To Be Selected...');
        }
    }
    handleEditData() {
     
        let idArr = [];
        idArr = this.props.inputVal.map(id => id);
     
        if (idArr.length > 0) {
            return <div className="table-responsive">
                <table className="fixed_header editscroll table table-striped table-bordered table-hover">
                    <tbody>
                        {this.generateRowsForEdit()}
                    </tbody>
                </table>
                <button onClick={this.handleSubmitEdit.bind(this)}>Submit</button>
            </div>
        }
}
    generateRowsForEdit() {
        if (this.state.modalOpenForOther){
        if (!(this.state.columns.length === 0 && this.state.client.length === 0)) {
            let columns = this.state.columns;
             let idArr = [];
            idArr = this.props.inputVal.map(id => id);
            if (!(columns.length === 0)) {
                const rows = [];
                rows.push(
                    <tr key={`row_`}>

                    </tr>
                )

                for (let i = 0; i < columns.length; i++) {
                    const columneValue = columns[i]
                    const value = columns[i].split('_').join(' ');
                    const ckd = value.toString();
                    if (!(ckd === 'UserID' || ckd === 'ClientID' || ckd === 'UserTypeID' || ckd === 'Label' || ckd === 'FirstName' || ckd === 'LastName')) {
                        rows.push(
                            <tr key={ckd}>
                                <th>{value}</th>
                                {/* {this.getanotherColumnMatch(columns[i], true)} */}
                                {/* {this.getanotherColumnTransposeForArmForMatch(columns[i], true)} */}
                                <td><label>{this.state.client[0][columns[i]]}</label></td>
                            </tr>
                        );
                    }
                    if (ckd === 'FirstName' || ckd === 'LastName') {
                        rows.push(
                            <tr key={ckd}>
                                <th>{value}</th>
                                {/* {this.getanotherColumnMatch(columns[i], true)} */}
                                {/* {this.getanotherColumnTransposeForArmForMatch(columns[i], true)} */}
                                {console.log('coulmn value',columneValue)}
                                <td><input name={columneValue} 
                                value={this.state.client[0][columns[i]]} onChange={this.handleTextBoxForEdit.bind(this)} /></td>
                            </tr>
                        );
                    }
                }

                return rows;
            }
        }
    }
    }
    handleTextBoxForEdit(e) {
        if (this.state.modalOpenForOther){
        let update = this.state.client;
        update[0][e.target.name] = e.target.value;
        update = update;
        this.setState({
            ...this.state,
            client: update
        }, () => {
        });
    }
}


    handleSubmitEdit(e) {
        if (this.state.modalOpenForOther){
        e.preventDefault();
        console.log(this.props);
       

        var formData = new FormData();
        formData.append("api_token", this.state.userData.API_Token);
        formData.append("edit_user_id",this.state.client[0].UserID);
        formData.append("FirstName",this.state.client[0].FirstName);
        formData.append("LastName",this.state.client[0].LastName);
        axios.request({
            method: "post",
            url: api.url+api.updateUser,
            data: formData,
            crossDomain: true,
            headers: { 'Content-Type': 'application/json' }
        }).then((result) => {
            let responseJson = result;
            console.log(responseJson.data.result);
            console.log(this.state.client[0])
            if(this.state.client[0].Label === "SP"){
            let url = api.url + api.getList + '?api_token=' + this.state.userData.API_Token + '&user_type=' + this.state.userData.label + '&user_type_to_get_data=SP';
            const { dispatch } = this.props;
            dispatch(listDataAction(url));
            }else{
                let url = api.url + api.getList + '?api_token=' + this.state.userData.API_Token + '&user_type=' + this.state.userData.label + '&user_type_to_get_data=CM';
                const { dispatch } = this.props;
                dispatch(listDataAction(url));
                    
            }
        }).catch(error => {
                console.log(error);
            });  
        // const {dispatch} = this.props;
        // dispatch(editForFirm(user))
        }
        this.setState({ showEditModal: false });
    }
    handleDeleteData(e){
        console.log('delete');
        e.preventDefault();
        let idArr = [];
        idArr = this.props.inputVal;
        for(let i=0;i<idArr.length;i++){
            firmArray = idArr[i].split('_');
            console.log(firmArray[1]);
        }
        idForFirm = firmArray[1];
        let url = api.url + api.deleteUser + '?api_token=' + this.state.userData.API_Token + '&delete_user_Id=' + idForFirm;
        axios.get(url).then(response => {
            idForFirm = [];
            firmArray = [];
            idArr = [];
            console.log(idArr);
            alert(response.data.message);

            if(this.state.client[0].Label === "SP"){
        
                let url = api.url + api.getList + '?api_token=' + this.state.userData.API_Token + '&user_type=' + this.state.userData.label + '&user_type_to_get_data=SP';
                const { dispatch } = this.props;
                dispatch(listDataAction(url));
                }else{
                    let url = api.url + api.getList + '?api_token=' + this.state.userData.API_Token + '&user_type=' + this.state.userData.label + '&user_type_to_get_data=CM';
                    const { dispatch } = this.props;
                    dispatch(listDataAction(url));
                        
                }
        }).catch(error => {
            console.log(error);
        });
    
           // const {dispatch} = this.props;
            // dispatch(filterRecouncilationFirm(url));
            idForFirm = [];
            firmArray = [];
            idArr = []; 
            this.props.deleteData(this.state.delete);
    }
    render() {
        return (
            <div className="col-xl-6 col-sm-6 pull-right">
                <div className="filter-search-div">
                    <div className="filter-nav-div ">
                        <div className="filt-nav-ul clearfix">
                            <ul>

                                <li>
                                    {/* <Icon name='edit' onClick={this.handleEditModal} /> */}
                                    <span  onClick={this.handleEditModal}
                                        className="ion-compose arrowpadding arrowpaddingb6" data-toggle="tooltip" data-placement="top" data-title="Edit" title="Edit">
                                    </span>
                                </li>
                                <li>
                                   {/* <Icon name='delete'  onClick={(e) => { if (window.confirm('Are you sure you wish to delete this item?')) this.handleDeleteData(e) } }/> */}
                                   <span  onClick={(e) => { if (window.confirm('Are you sure you wish to delete this item?')) this.handleDeleteData(e) } }
                                        className="ion-trash-a arrowpadding arrowpaddingb6" data-toggle="tooltip" data-placement="top" data-title="Edit" title="Edit">
                                    </span>
                               
                                </li>
                            </ul>
                        </div>
                    </div>
                    {/* <!-- end of filter-nav-div--> */}
                    <div className="search-data-div">
                        <div className="clearfix">
                            <form>
                                <div className="search-col-1"><input type="text" className="form-control" placeholder="Search" /></div>
                                <div className="search-col-6"><button className="btn sbtn">Search</button></div>
                            </form>
                        </div>
                    </div>
                </div>
                <Modal className={`${this.state.showEditModalClass}`} show={this.state.showEditModal} onHide={this.handleEditCloseModal}>
                    <Modal.Header className="modal-header1" closeButton>
                        <Modal.Title className="title-files">Edit</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="modal-new1">
                            <div className="row">
                                <div className="col-xl-12">
                                    <div className="card m-b-20">
                                        <div className="card-body">
                                            {this.handleEditData()}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleEditCloseModal}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}
function mapStateToProps(state) {
    const { client } = state.listData;
    const { columns } = state.listData;
    return {
        client,
        columns
    };
}
export default connect(mapStateToProps)(FilterForList);