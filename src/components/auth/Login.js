import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Logo from '../../utils/images/logo.png'
import { connect } from 'react-redux';
import { userActions } from '../../actions/loginAction';
class Login extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            redirectToReferrer: false,
            formErrors: { email: '', password: '' },
            emailValid: false,
            passwordValid: false,
            formValid: false,
            loggedIn: false,
            userData: '',
        };
        this.login = this.login.bind(this);
        this.handleUserInput = this.handleUserInput.bind(this);
        // this.onChange = this.onChange.bind(this);

    }
    handleUserInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value },
            () => { this.validateField(name, value) });
    }

    login() {
        if (this.state.email && this.state.password) {
            let user = {
                email: this.state.email,
                password: this.state.password,
            };
            const { dispatch } = this.props;
            dispatch(userActions.login(user));
        }
        setTimeout(() => {
            const user = JSON.parse(sessionStorage.getItem('userData'));
            this.setState({ userData: user });
        }, 1000);
    }



    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;

        switch (fieldName) {
            case 'email':
                emailValid = value.length >= 5;
                fieldValidationErrors.email = emailValid ? '' : ' is too short';
                break;
            case 'password':
                passwordValid = value.length >= 5;
                fieldValidationErrors.password = passwordValid ? '' : ' is too short';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid,
            passwordValid: passwordValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({ formValid: this.state.emailValid && this.state.passwordValid });
    }
    errorClass(error) {
        return (error.length === 0 ? '' : 'has-error');
    }
    componentDidMount() {
    
    }
    render() {
        if (this.state.userData) {
            return <Redirect exact push to={{ pathname: '/insights' }} />
        }
    
        return (
            <div className="wrapper-page">
                <div className="card">
                    <div className="card-body">
                        <h3 className="text-center m-0">
                            <a href="insights.html" className="logo logo-admin">
                                <img src={Logo} alt="RegX" />
                            </a>
                        </h3>
                        <div className="p-3">
                            <h4 className="text-muted font-18 m-b-5 text-center">Welcome Back !</h4>
                            <p className="text-muted text-center">Sign in to continue to Regx.</p>
                            <div className="form-horizontal m-t-30">
                                <div className={`form-group ${this.errorClass(this.state.formErrors.email)}`}>
                                    <label htmlFor="username">Username</label>
                                    <input type="text" name="email" onChange={(event) => this.handleUserInput(event)} value={this.state.email} className="form-control" id="username" placeholder="Enter username" />
                                <p>{this.state.formErrors.email}</p>
                                </div>
                                <div className={`form-group ${this.errorClass(this.state.formErrors.password)}`}>
                                    <label htmlFor="userpassword">Password</label>
                                    <input type="password" name="password" onChange={(event) => this.handleUserInput(event)} value={this.state.password} className="form-control" id="userpassword" placeholder="Enter password" />
                                    <p>{this.state.formErrors.password}</p>
                                </div>
                                <div className="form-group row m-t-20">
                                    <div className="col-6">
                                        <div className="custom-control custom-checkbox">
                                            <input type="checkbox" className="custom-control-input" id="customControlInline" />
                                            <label className="custom-control-label" htmlFor="customControlInline">Remember me</label>
                                        </div>
                                    </div>
                                    <div className="col-6 text-right">
                                        <button className="btn btn-primary w-md waves-effect waves-light" onClick={this.login} type="submit">Log In</button>
                                    </div>
                                </div>
                                <div className="form-group m-t-10 mb-0 row">
                                    <div className="col-12 m-t-20">
                                        <a href="/forgot-password" className="text-muted">
                                            <i className="mdi mdi-lock"></i> Forgot your password?
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="m-t-40 text-center">
                    <p>&copy; 2018 Regx.</p>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggedIn } = state.authentication;
   
    return {
        loggedIn,
    };
}

export default connect(mapStateToProps)(Login);