import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from '../../navigation/Header';
import Footer from '../../navigation/Footer';
import Filter from '../../navigation/Filter';
// import { Line, Bar } from 'react-chartjs-2';
import axios from 'axios'
import api from '../../../helpers/api'
import moment from 'moment';
import Helmet from 'react-helmet';
import recouncilation from '../../../actions/Completeness/FirmArmActions/recouncilationAction';
import armrecouncilation from '../../../actions/Completeness/FirmArmActions/armRecouncilationAction';
import toDate1 from '../../../actions/Completeness/dateAction';


import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

import { formatDate, parseDate } from 'react-day-picker/moment';
let ckdArr = [];
let ckdArrForArm = [];
let plainOptionsFrm = [];
let plainOptionsArm = [];
let uniqueNames = [];
let uniqueNames2 = [];
let ckAdrrNewArm = [];
let ckAdrrNew = [];
const bgstyle = {
    backgroundColor: '#ffdede'
};
const dataMsg = {
    width: '1000px',
    color: '#5b626b',
    backgroundColor: '#ffffff',
    border: '0px'
}
class FirmArm extends Component {
    constructor() {
        super();
        var tempDate = new Date();
        var firstDay = new Date(tempDate.getFullYear(), tempDate.getMonth(), 1);
        //var firstDay = new Date(tempDate.getFullYear(), 10, 1);

        //chaned tempdate to tempdate1 by krupal 20 dec
        var tempDate1 = new Date();
        var date = tempDate1.getFullYear() + '-' + (tempDate1.getMonth() + 1) + '-' + tempDate1.getDate();
        var importdate = new Date();
        this.state = {
            recouncilationdata: [],
            recouncilationColumns: [],
            armrecouncilationdata: [],
            armrecouncilationColumns: [],
            userData: JSON.parse(sessionStorage.getItem('userData')),
            loading: false,
            ARMReportedLoading: false,
            date: date,
            from: new Date(),
            to: new Date(),
            breakStatus: '0',
            transpose: '',
            refresh: '',
            alltrancation: '',
            id: [],
            idForArm: [],
            singleCheckbox: [],
            checkedListArm: [],
            checkedListFrm: [],
            checkAll: false,
            isAllSelectedFrm: false,
            isAllSelectedArm: false,
            dataIdForFirm: [],
            match: '',
            showMatchModal: '',
            loder: false,
            matchloader: false,
            focusmodalClose: false,
            allTransactionValue: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleFromChange = this.handleFromChange.bind(this);
        this.handleToChange = this.handleToChange.bind(this);
        this.handleTransaction = this.handleTransaction.bind(this);
        this.handleTrans = this.handleTrans.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
        this.handleSingleCheckBox = this.handleSingleCheckBox.bind(this);
        this.handleSingleCheckBoxForArm = this.handleSingleCheckBoxForArm.bind(this);
        this.handleLoder = this.handleLoder.bind(this);
        this.handleLoderForMatch = this.handleLoderForMatch.bind(this);
        this.handleMatch = this.handleMatch.bind(this);
        this.handleOpenmatch = this.handleOpenmatch.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
    }
    handleLoder() {
        return <div className="abs_loader"> <div className="loader"></div></div>
    }
    handleLoderForMatch() {
        setTimeout(() => {
            this.setState({ loder: false });
        }, 2000)
        return <div className="abs_loader"><div className="loader"></div></div>

    }
    handleFocus(focusValue) {

    }
    handleMatch(matchValue) {

        this.setState({ loder: matchValue }, () => {

        });
    }
    handleOpenmatch(matchopenvalue) {
        this.setState({ showMatchModal: matchopenvalue }, () => {

        });


    }

    handelList = (breakStatus) => {
        this.setState({ ARMReportedLoading: false });
        this.setState({ loading: false })
        var from = new Date(this.state.from);
        from = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        //from = from.getFullYear() + '-' + 10 + '-' + from.getDate();
        var to;

        setTimeout(() => {
            to = this.props.toDate;
            to = to.getFullYear() + '-' + (to.getMonth() + 1) + '-' + to.getDate();
            let url = api.url + api.getRecouncilation + '?api_token=' + this.state.userData.API_Token + '&clientName=' + this.state.userData.ClientName + '&ReconciliationType=firm-arm&RecAttribute=RecordMatching&FileFromDate=' + from + '&FileToDate=' + to + '&breackStatus=' + breakStatus;
            const { dispatch } = this.props;
            dispatch(recouncilation(url));
            this.setState({ ARMReportedLoading: true });
        }, 1000);
        //to = to.getFullYear() + '-' + 10 + '-' + to.getDate();
        setTimeout(() => {
            this.setState({ loading: true });
        }, 3500);
        uniqueNames2 = [];
        this.setState({ id: uniqueNames2 });
        uniqueNames = [];
        this.setState({ idForArm: uniqueNames });
        plainOptionsFrm = [];
        plainOptionsFrm = [];
        ckAdrrNewArm = [];
        ckAdrrNew = [];

    }

    handelListForArm = (breakStatus) => {

        setTimeout(() => {

        }, 1000);

        this.setState({ ARMReportedLoading: false });
        var from = new Date(this.state.from);
        from = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        var to;
        this.setState({ loading: false });
        setTimeout(() => {
            to = this.props.toDate;
            to = to.getFullYear() + '-' + (to.getMonth() + 1) + '-' + to.getDate();


            let url = api.url + api.getRecouncilation + '?api_token=' + this.state.userData.API_Token + '&clientName=' + this.state.userData.ClientName + '&ReconciliationType=arm-firm&RecAttribute=RecordMatching&FileFromDate=' + from + '&FileToDate=' + to + '&breackStatus=' + breakStatus;
            const { dispatch } = this.props;
            dispatch(armrecouncilation(url));
            this.setState({ ARMReportedLoading: true });

        }, 1000);
        setTimeout(() => {
            this.setState({ loading: true });
        }, 2000);
        uniqueNames2 = [];
        this.setState({ id: uniqueNames2 });
        uniqueNames = [];
        this.setState({ idForArm: uniqueNames });
        plainOptionsFrm = [];
        plainOptionsFrm = [];
    }

    handleChange(date) {
        var tempDate = new Date(date);
        //change date to dateForState by krupal 20 dec
        var dateForState = tempDate.getFullYear() + '-' + (tempDate.getMonth() + 1) + '-' + tempDate.getDate();

        this.setState({
            date: dateForState
        });
        this.handelList(0);
        this.handelListForArm(0);

    }
    componentDidMount() {


    }
    componentWillMount() {
        this.setState({ loading: false });
        // this.setState({breakStatus:'0'});
        this.handelList(0);
        this.handelListForArm(0);
        setTimeout(() => {
            this.setState({ loading: true });
        }, 2000);
        const { dispatch } = this.props;
        dispatch(toDate1(this.state.to));
    }

    columnsRecouncilation(columnsRecouncilationmap) {
        let columns = [];
        columns.push(

        )
        Object.keys(columnsRecouncilationmap).map(function (data) {
            const valueckd = data.toString();
            if (!(valueckd === 'BreakStatus' || valueckd === 'DataId')) {
                columns.push(<td key={valueckd}>{columnsRecouncilationmap[data]}</td>)
            }

        });

        return columns;
    }

    showFromMonth() {
        const { from } = this.state;
        const { toDate } = this.props;
        const { to } = toDate;
        if (!from) {
            return;
        }
        if (moment(to).diff(moment(from), 'months') < 2) {
            this.to.getDayPicker().showMonth(from);
        }
    }
    handleFromChange(from) {
        // Change the from date and focus the "to" input field
        setTimeout(() => {
            this.setState({ from });
        }, 1000);
        //this.handelList()

    }
    handleToChange(to) {
    
        const { dispatch } = this.props;
        dispatch(toDate1(to));
        this.showFromMonth();
        this.handelList(0);
        this.handelListForArm(0);
    }

    getanotherColumnTranspose(column, status) {
        // Add checkbox type  and condition by krupal 20 dec
        let columnName = column;
        let columns = [];
        var columnsRecouncilationmap = this.props.recouncilationdata
        if (status) {
            for (let i = 0; i < columnsRecouncilationmap.length; i++) {
                columns.push(
                    columnsRecouncilationmap[i].BreakStatus === '0' ?
                        <td key={`col_${columnsRecouncilationmap[i].DataId}`} style={bgstyle}>{columnsRecouncilationmap[i][columnName]}</td>
                        :
                        <td key={`col_${columnsRecouncilationmap[i].DataId}`}>{columnsRecouncilationmap[i][columnName]}</td>
                )
            }
        } else {
            for (let i = 0; i < columnsRecouncilationmap.length; i++) {
                let checked = false;
                if (this.state.id.indexOf(`column_${columnsRecouncilationmap[i].DataId}`) >= 0 || this.state.isAllSelectedFrm) {
                    checked = true;
                }
                plainOptionsFrm[i] = (`column_${columnsRecouncilationmap[i].DataId}`);
                columns.push(
                    columnsRecouncilationmap[i].BreakStatus === '0' ?
                        <td key={`row_${i}`} style={bgstyle}><div className="checkbox-wrapper"><input id={`column_${columnsRecouncilationmap[i].DataId}`} checked={checked} name="checkedAll" onChange={this.handleSingleCheckBox} type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                        :
                        <td key={`row_${i}`}><div className="checkbox-wrapper"><input id={`column_${columnsRecouncilationmap[i].DataId}`} checked={checked} name="checkedAll" onChange={this.handleSingleCheckBox} type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>

                )
            }
        }
        return columns;
    }

    generateRowsWhenTranspose() {

        if (this.state.transpose) {
            let columns = this.props.recouncilationColumns;
            if (!(columns.length === 0)) {
                const rows = [];
                rows.push(
                    //Add checkbox type by krupal by 20 dec
                    <tr key={`row_`}>
                        <th ><div className="checkbox-wrapper"> <input id="chk20" checked={this.state.isAllSelectedFrm} onChange={(e) => this.onCheckBoxChange('all', e.target.checked, 'frm')} type="checkbox" /><label htmlFor="chk20" className="toggle"></label></div></th>
                        {this.getanotherColumnTranspose(0, false)}
                    </tr>

                )
                for (let i = 0; i < columns.length; i++) {
                    const value = columns[i].split('_').join(' ');
                    const ckd = value.toString();
                    if (!(ckd === 'BreakStatus')) {
                        rows.push(
                            <tr key={ckd} >
                                <th>{value}</th>
                                {this.getanotherColumnTranspose(columns[i], true)}
                            </tr>

                        );
                    }
                }
                return rows;
            }
        }
    }

    getanotherColumnTransposeForArm(column, status) {
        // Add checkbox type  and condition by krupal 20 dec
        let columnName = column;
        let columns = [];
        var columnsRecouncilationmap = this.props.armrecouncilationdata
        if (status) {
            for (let i = 0; i < columnsRecouncilationmap.length; i++) {

                columns.push(
                    columnsRecouncilationmap[i].BreakStatus === '0' ?
                        <td key={`column_${columnsRecouncilationmap[i].DataId}`} style={bgstyle}>{columnsRecouncilationmap[i][columnName]}</td>
                        :
                        <td key={`column_${columnsRecouncilationmap[i].DataId}`}>{columnsRecouncilationmap[i][columnName]}</td>
                )
            }
        } else {
            for (let i = 0; i < columnsRecouncilationmap.length; i++) {
                var checked = false;
                if (this.state.idForArm.indexOf(`column_${columnsRecouncilationmap[i].DataId}`) >= 0 || this.state.isAllSelectedArm) {
                    checked = true;
                }
                plainOptionsArm[i] = (`column_${columnsRecouncilationmap[i].DataId}`);
                columns.push(
                    columnsRecouncilationmap[i].BreakStatus === '0' ?
                        <td key={`row_${i}`} style={bgstyle}><div className="checkbox-wrapper"><input id={`column_${columnsRecouncilationmap[i].DataId}`} type="checkbox" checked={checked} onChange={this.handleSingleCheckBoxForArm} onClick={this.handleSingleCheckBoxArm} /> <label htmlFor="chk20" className="toggle"></label></div></td>
                        :
                        <td key={`row_${i}`} ><div className="checkbox-wrapper"><input id={`column_${columnsRecouncilationmap[i].DataId}`} type="checkbox" checked={checked} onChange={this.handleSingleCheckBoxForArm} onClick={this.handleSingleCheckBoxArm} /> <label htmlFor="chk20" className="toggle"></label></div></td>)
            }
        }
        return columns;
    }


    generateRowsWhenTransposeForArm() {
        // Add checkbox type by krupal
        if (this.state.transpose) {
            let columns = this.props.armrecouncilationColumns;
            if (!(columns.length === 0)) {
                const rows = [];
                rows.push(

                    <tr key={`row_`}>
                        {}
                        <th><div className="checkbox-wrapper">
                            <input id="chk20" checked={this.state.isAllSelectedArm} onChange={(e) => this.onCheckBoxChange('all', e.target.checked, 'arm')} type="checkbox" /><label htmlFor="chk20" className="toggle"></label></div></th>
                        {this.getanotherColumnTransposeForArm(0, false)}
                    </tr>

                )
                for (let i = 0; i < columns.length; i++) {
                    const value = columns[i].split('_').join(' ');
                    const ckd = value.toString();
                    if (!(ckd === 'BreakStatus')) {
                        rows.push(
                            <tr key={ckd} >
                                <th>{value}</th>
                                {this.getanotherColumnTransposeForArm(columns[i], true)}
                            </tr>

                        );
                    }
                }
                return rows;
            }
        }
    }

    generateRows = (recouncilationdata) => {
        const rows = [];
        for (let i = 0; i < recouncilationdata.length; i++) {
            let checked = false;
            if (this.state.id.indexOf(`column_${recouncilationdata[i].DataId}`) >= 0 || this.state.isAllSelectedFrm) {
                checked = true;
            }
            plainOptionsFrm[i] = (`column_${recouncilationdata[i].DataId}`);

            rows.push(
                recouncilationdata[i].BreakStatus === '0' ?
                    <tr key={`row_${i}`} style={bgstyle}>
                        <td><div className="checkbox-wrapper"><input id={`column_${recouncilationdata[i].DataId}`} checked={checked} name="checkedAll" onChange={this.handleSingleCheckBox} type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                        {this.columnsRecouncilation(recouncilationdata[i])}
                    </tr> :
                    <tr key={`row_${i}`}>
                        <td><div className="checkbox-wrapper"><input id={`column_${recouncilationdata[i].DataId}`} checked={checked} name="checkedAll" onChange={this.handleSingleCheckBox} type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                        {this.columnsRecouncilation(recouncilationdata[i])}
                    </tr>
            );
        }
        return rows;
    }
    armcolumnsRecouncilation(armcolumnsRecouncilationmap) {
        let columns = [];
        columns.push(

        )
        Object.keys(armcolumnsRecouncilationmap).map(function (data) {
            const valueckd = data.toString();
            if (!(valueckd === 'BreakStatus' || valueckd === 'DataId')) {
                columns.push(<td key={valueckd}>{armcolumnsRecouncilationmap[data]}</td>)
            }
        });
        return columns;
    }

    generateRowsArm = (armrecouncilationdata) => {
        const rows = [];

        for (let i = 0; i < armrecouncilationdata.length; i++) {

            var checked = false;
            if (this.state.idForArm.indexOf(`column_${armrecouncilationdata[i].DataId}`) >= 0 || this.state.isAllSelectedArm) {
                checked = true;
            }
            plainOptionsArm[i] = (`column_${armrecouncilationdata[i].DataId}`);


            rows.push(
                armrecouncilationdata[i].BreakStatus === '0' ?
                    <tr key={`row_${i}`} style={bgstyle}>
                        <td><div className="checkbox-wrapper" ><input id={`column_${armrecouncilationdata[i].DataId}`} checked={checked} name="checkedAll" onChange={this.handleSingleCheckBoxForArm} type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                        {this.armcolumnsRecouncilation(armrecouncilationdata[i])}
                    </tr> :
                    <tr key={`row_${i}`} >
                        <td><div className="checkbox-wrapper"><input id={`column_${armrecouncilationdata[i].DataId}`} checked={checked} name="checkedAll" onChange={this.handleSingleCheckBoxForArm} type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                        {this.armcolumnsRecouncilation(armrecouncilationdata[i])}
                    </tr>
            );
        }

        return rows;
    }

    handleSingleCheckBoxForArm(e) {
        ckAdrrNewArm = [];
        ckAdrrNewArm = this.state.idForArm;  //this.state.id;
        if (this.state.isAllSelectedArm) {
            ckAdrrNewArm = plainOptionsArm;
            //remove parts declaration by krupal 20 dec
            var ckAdrrNewChecked = ckAdrrNewArm.filter(function (ele) {
                if (ele !== e.target.id) {
                    return ele;
                }

            });
        } else {
            if (e.target.checked) {
                const parts = e.target.id.split("_");
                ckAdrrNewArm[parts[1]] = e.target.id;
                ckAdrrNewChecked = ckAdrrNewArm

            } else {
                //remove parts declaration by krupal 20 dec
                ckAdrrNewChecked = ckAdrrNewArm.filter(function (ele) {
                    if (ele !== e.target.id) {
                        return ele;
                    }
                });
            }
        }
        ckdArrForArm = ckAdrrNewChecked;
        uniqueNames = Array.from(new Set(ckdArrForArm));
        //add condition for remove undefined by krupal 20 dec
        var index = uniqueNames.indexOf(undefined);
        if (index > -1) {
            uniqueNames.splice(index, 1);
        }
        this.setState({ idForArm: uniqueNames, isAllSelectedArm: false });

    }
    handleSingleCheckBox(e) {
        ckAdrrNew = [];
        ckAdrrNew = this.state.id;
        //remove parts declaration which is not used by krupal 20 dec
        if (this.state.isAllSelectedFrm) {
            ckAdrrNew = plainOptionsFrm;
            var ckAdrrNewChecked = ckAdrrNew.filter(function (ele) {
                if (ele !== e.target.id) {
                    return ele;
                }

            });
        } else {

            if (e.target.checked) {
                const parts = e.target.id.split("_");
                ckAdrrNew[parts[1]] = e.target.id;
                ckAdrrNewChecked = ckAdrrNew
            } else {
                ckAdrrNewChecked = ckAdrrNew.filter(function (ele) {
                    if (ele !== e.target.id) {
                        return ele;
                    }

                });
            }
        }
        ckdArr = ckAdrrNewChecked;
        uniqueNames2 = Array.from(new Set(ckdArr));
        //add condition for remove undefined by krupal 20 dec
        var index = uniqueNames2.indexOf(undefined);
        if (index > -1) {
            uniqueNames2.splice(index, 1);
        }
        this.setState({ id: uniqueNames2, isAllSelectedFrm: false }, () => {
           
        });

    }

    handleTrans(transValue) {
        this.setState({ transpose: transValue });

    }
    handleRefresh(RefreshValue) {
        this.setState({ refresh: RefreshValue });
        setTimeout(() => {
            if (this.state.allTransactionValue === true) {
                this.handelList('');
                this.handelListForArm('');
            } else {
                this.handelList(0);
                this.handelListForArm(0);
            }
        }, 1000)

    }
    handleTransaction(transactionValue) {
        this.setState({ allTransactionValue: transactionValue }, () => {
           
        });
    
        if (transactionValue === true) {
            setTimeout(() => {
                this.handelList('');
                this.handelListForArm('');

            }, 1000)
        } else {
            setTimeout(() => {
                this.handelList(0);
                this.handelListForArm(0);

            }, 1000)
        }
    }

    onCheckBoxChange(checkName, isChecked, type) {
        const checked = isChecked;
        if (type === 'frm') {
            this.setState({
                isAllSelectedFrm: checked,
                id: (checked ? plainOptionsFrm : [])
            });

        }
        if (type === 'arm') {
            this.setState({
                isAllSelectedArm: checked,
                idForArm: (checked ? plainOptionsArm : [])
            });

        }


    }

    render() {

        const DataAvailableMsg = 'Transaction not available for this date, please use different date.';

        var tableHeadwithColumns;
        let columns = this.props.recouncilationColumns;
        if (!(columns.length === 0)) {
            tableHeadwithColumns = columns.map(function (data) {

                const value = data.split('_').join(' ');
                if (!(value === 'BreakStatus' || value === 'DataId')) {
                    return <th key={value.toString()}>{value}</th>
                }
            });
        }
        var tableHeadwithColumnsArm;
        let columnsarm = this.props.armrecouncilationColumns;
        if (!(columnsarm.length === 0)) {
            tableHeadwithColumnsArm = columnsarm.map(function (data) {

                const value = data.split('_').join(' ');
                if (!(value === 'BreakStatus' || value === 'DataId')) {
                    return <th key={value.toString()}>{value}</th>
                }
            });
        }

        const { toDate } = this.props;

        const to = toDate;
        const { from } = this.state;
        const modifiers = { start: from, end: to };
        return (

            <div>
                <div>
                    <Header activeClass="firmArm" />
                    <div className="wrapper">
                        <div className="container-fluid">
                            {/* <!-- Page-Title --> */}
                            <div className="row">
                                <div className="col-xl-6 col-sm-6">
                                    <div className="page-title-box">
                                        <h4 className="page-title">MiFD II - Completeness > Firm-ARM Reconciliation</h4>
                                    </div>
                                </div>
                                <Filter ontransClick={this.handleTrans}
                                    onTransactionclick={this.handleTransaction}
                                    onRefreshClick={this.handleRefresh}
                                    inputVal={this.state.id}
                                    inputValForArm={this.state.idForArm}
                                    isAllselectForFirm={this.state.isAllSelectedFrm}
                                    isAllselectForArm={this.state.isAllSelectedArm}
                                    isMatchOpen={this.state.matchloader}
                                    onMatchClick={this.handleMatch}
                                    matchmodal={this.handleOpenmatch}
                                    closeFocus={this.handleFocus}

                                //datacolForArm={this.state.plainOptionsFrm}
                                />
                            </div>
                        </div>
                        {/* lodder start */}

                        {this.state.loder === true ?
                            <div>
                                {this.handleLoderForMatch()}
                            </div>
                            :

                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-xl-12">
                                        <div className="card m-b-20">

                                            {this.state.loading === true ?

                                                <div className="card-body">
                                                    <h4 className="mt-0 m-b-10 header-title float-left">FIRM Transactions</h4>

                                                    <div className="InputFromTo">
                                                        <DayPickerInput
                                                            value={from}
                                                            className="date-input"
                                                            placeholder="From"
                                                            format="LL"
                                                            formatDate={formatDate}
                                                            parseDate={parseDate}
                                                            dayPickerProps={{
                                                                selectedDays: [from, { from, to }],
                                                                disabledDays: { after: to },
                                                                toMonth: to,
                                                                modifiers,
                                                                numberOfMonths: 2,
                                                                onDayClick: () => this.to.getInput().focus(),
                                                            }}
                                                            onDayChange={this.handleFromChange}
                                                        />
                                                        <span className="InputFromTo-to">
                                                            <DayPickerInput
                                                                ref={el => (this.to = el)}
                                                                value={to}
                                                                className="date-input"
                                                                placeholder="To"
                                                                format="LL"
                                                                formatDate={formatDate}
                                                                parseDate={parseDate}
                                                                dayPickerProps={{
                                                                    selectedDays: [from, { from, to }],
                                                                    disabledDays: { before: from },
                                                                    modifiers,
                                                                    month: from,
                                                                    fromMonth: from,
                                                                    numberOfMonths: 2,
                                                                }}
                                                                onDayChange={this.handleToChange}
                                                            />
                                                        </span>
                                                        <Helmet>
                                                            <style>{`

                                        .InputFromTo { margin:0 0 10px 25%; float:left; padding:0; text-align:center;  display:flex;}
                                        .DayPickerInput-OverlayWrapper { font-size:12px; left:0; right:auto;}
                                        .InputFromTo .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
                                            background-color: #f0f8ff !important;
                                            color: #4a90e2; font-size:12px;
                                        }
                                        .InputFromTo .DayPicker-Day {
                                            border-radius: 0 !important; font-size:12px;
                                        }
                                        .InputFromTo .DayPicker-Day--start {
                                            border-top-left-radius: 50% !important; font-size:12px;
                                            border-bottom-left-radius: 50% !important;
                                        }
                                        .InputFromTo .DayPicker-Day--end {
                                            border-top-right-radius: 50% !important; font-size:12px;
                                            border-bottom-right-radius: 50% !important;
                                        }
                                        .InputFromTo .DayPickerInput-Overlay {
                                            width: 520px; font-size:12px;
                                        }
                                        .InputFromTo-to .DayPickerInput-Overlay {
                                            margin-left: 0;
                                        }
                                        `}</style>
                                                        </Helmet>
                                                    </div>

                                                    <div>
                                                        <div>
                                                            {!this.state.ARMReportedLoading
                                                                ? <div><i className="fa fa-spin fa-fw fa-spinner"></i> Loding</div> :

                                                                <div>

                                                                    {this.state.transpose && this.props.recouncilationdata.length > 0 ?
                                                                        <div className="table-responsive">
                                                                            <table className="fixed_header table table-striped table-bordered table-hover">
                                                                                <tbody>
                                                                                    {this.generateRowsWhenTranspose()}
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        :
                                                                        <div className="table-responsive">
                                                                            {this.props.recouncilationdata.length ?
                                                                                <table className="fixed_header table table-striped table-bordered table-hover">

                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th>
                                                                                                <div className="checkbox-wrapper">
                                                                                                    <input id="chk20" checked={this.state.isAllSelectedFrm} onChange={(e) => this.onCheckBoxChange('all', e.target.checked, 'frm')} type="checkbox" />
                                                                                                    <label htmlFor="chk20" className="toggle"></label>
                                                                                                </div>
                                                                                            </th>
                                                                                            {tableHeadwithColumns}
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        {this.generateRows(this.props.recouncilationdata)}
                                                                                    </tbody>
                                                                                </table>

                                                                                :
                                                                                <div>
                                                                                    <table className="fixed_header table table-striped table-bordered table-hover">
                                                                                        <thead>
                                                                                        </thead>

                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <th style={dataMsg}><h4 className="mt-0 m-b-10 header-title float-left">{DataAvailableMsg}</h4></th>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            }
                                                                        </div>
                                                                    }
                                                                </div>
                                                            }
                                                        </div>
                                                        <h4 className="mt-0 m-b-10 header-title float-left">ARM Transactions</h4>
                                                        <div>
                                                            {this.state.transpose && this.props.armrecouncilationdata.length > 0 ?
                                                                <div className="table-responsive">
                                                                    <table className="fixed_header table table-striped table-bordered table-hover">
                                                                        <tbody>
                                                                            {this.generateRowsWhenTransposeForArm()}
                                                                        </tbody>

                                                                    </table>
                                                                </div>
                                                                :
                                                                <div className="table-responsive">
                                                                    {this.props.armrecouncilationdata.length ?
                                                                        <table className="fixed_header table table-striped table-bordered table-hover">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>
                                                                                        <div className="checkbox-wrapper">
                                                                                            <input id="chk20" checked={this.state.isAllSelectedArm} onChange={(e) => this.onCheckBoxChange('all', e.target.checked, 'arm')} type="checkbox" /> <label htmlFor="chk20" className="toggle"></label>
                                                                                        </div>
                                                                                    </th>
                                                                                    {tableHeadwithColumnsArm}
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                {this.generateRowsArm(this.props.armrecouncilationdata)}
                                                                            </tbody>
                                                                        </table>
                                                                        :
                                                                        <div>
                                                                            <table className="fixed_header table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                </thead>

                                                                                <tbody>
                                                                                    <tr>
                                                                                        <th style={dataMsg}><h4 className="mt-0 m-b-10 header-title float-left">{DataAvailableMsg}</h4></th>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    }
                                                                </div>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                :
                                                <div>
                                                    {this.handleLoder()}
                                                </div>
                                            }
                                        </div>
                                        {/* <!-- end of table responsive --> */}

                                    </div>

                                </div>

                            </div>


                        }
                        {/* lodder end */}
                    </div>
                    {/* <!-- end wrapper --><!-- Footer --> */}
                    <Footer />
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    const { recouncilationdata } = state.recouncilation
    const { recouncilationColumns } = state.recouncilation
    const { armrecouncilationdata } = state.armrecouncilation
    const { armrecouncilationColumns } = state.armrecouncilation
    const { toDate } = state.dateReducer
    return {
        recouncilationdata,
        recouncilationColumns,
        armrecouncilationdata,
        armrecouncilationColumns,
        toDate,
    };
}
export default connect(mapStateToProps)(FirmArm);
