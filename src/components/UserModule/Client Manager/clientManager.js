import React, { Component } from 'react';
import Header from '../../navigation/Header';
import api from '../../../helpers/api';
import edit from '../../../utils/images/icons/edit.png';
import { connect } from 'react-redux';
import listDataAction from '../../../actions/List/listdataAction';
import { Button, Modal } from 'react-bootstrap';
import { Dropdown } from "semantic-ui-react";
import FilterForList from '../../navigation/FilterForList';
import axios from 'axios';
let ckdArr = [];
let ckdArrForArm = [];
let plainOptionsFrm = [];
let plainOptionsArm = [];
let uniqueNames = [];
let uniqueNames2 = [];
let ckAdrrNewArm = [];
let ckAdrrNew = [];
let client = [];

class clientManager extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: JSON.parse(sessionStorage.getItem('userData')),
            client: '',
            columns: '',
            editId: '',
            showModal: false,
            showModalClass: '',
            id: [],
            isAllSelectedFrm: false,
            clientList: '',
            username: '',
            password: '',
            fname: '',
            lname: '',
            usertype: 'Client Manager'
        };
        this.handelList = this.handelList.bind(this);
        this.handleSingleCheckBox = this.handleSingleCheckBox.bind(this);
        this.handleAddData = this.handleAddData.bind(this);
        this.handleFocusCloseModal = this.handleFocusCloseModal.bind(this);
        this.handleChangeReviewersAmount = this.handleChangeReviewersAmount.bind(this);
        this.handleChangeValue = this.handleChangeValue.bind(this);
        this.handleAddClientSubmit = this.handleAddClientSubmit.bind(this);
    }
    handleCloseModal() {
        this.setState({ showModal: false, showModalClass: '' });
    }
    handelList = () => {
        let url = api.url + api.getList + '?api_token=' + this.state.userData.API_Token + '&user_type=' + this.state.userData.label + '&user_type_to_get_data=CM' + '&username=' + this.state.userData.UserName;
        const { dispatch } = this.props;
        dispatch(listDataAction(url));
    }
    handleClientList = () => {
        let url = api.url + api.getClientList + '?api_token=' + this.state.userData.API_Token;
        axios.get(url).then((responce) => {
          
            responce.data.result.forEach(element => {
                client.push(element.ClientName);
            });
        });
      
    }

    componentDidMount() {
        this.handelList();
        this.handleClientList();
    }
    columnsgetData(getColumnData) {
        let columns = [];

        Object.keys(getColumnData).map(function (data, number) {
            const value = data;
            if (!(value === 'UserID' || value === 'ClientID' || value === 'UserTypeID' || value === 'Label')) {
                columns.push(<td key={number}>{getColumnData[data]}</td>)
            }
        });
        return columns;
    }
    generateRows = (client) => {
        const rows = [];

        for (let i = 0; i < client.length; i++) {
            let checked = false;
            if (this.state.id.indexOf(`column_${client[i].UserID}`) >= 0 || this.state.isAllSelectedFrm) {
                checked = true;
            }
            plainOptionsFrm[i] = (`column_${client[i].UserID}`);
            rows.push(
                <tr key={`row_${i}`} >
                    <td> <div className="checkbox-wrapper"><input id={`column_${client[i].UserID}`} checked={checked} name="checkedAll" onChange={this.handleSingleCheckBox} type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                    {this.columnsgetData(client[i])}
                </tr>
            );
        }

        return rows;
    }
    handleSingleCheckBox(e) {
        ckAdrrNew = [];
        ckAdrrNew = this.state.id;
        //remove parts declaration which is not used by krupal 20 dec
        if (this.state.isAllSelectedFrm) {
            ckAdrrNew = plainOptionsFrm;
            var ckAdrrNewChecked = ckAdrrNew.filter(function (ele) {
                if (ele !== e.target.id) {
                    return ele;
                }

            });
        } else {

            if (e.target.checked) {
                const parts = e.target.id.split("_");
                ckAdrrNew[parts[1]] = e.target.id;
                ckAdrrNewChecked = ckAdrrNew
            } else {
                ckAdrrNewChecked = ckAdrrNew.filter(function (ele) {
                    if (ele !== e.target.id) {
                        return ele;
                    }

                });
            }
        }
        ckdArr = ckAdrrNewChecked;
        uniqueNames2 = Array.from(new Set(ckdArr));
        //add condition for remove undefined by krupal 20 dec
        var index = uniqueNames2.indexOf(undefined);
        if (index > -1) {
            uniqueNames2.splice(index, 1);
        }
        this.setState({ id: uniqueNames2, isAllSelectedFrm: false }, () => {
          
        });
    }
    onCheckBoxChange(checkName, isChecked, type) {
        const checked = isChecked;
        if (type === 'frm') {
            this.setState({
                isAllSelectedFrm: checked,
                id: (checked ? plainOptionsFrm : [])
            });

        }
        if (type === 'arm') {
            this.setState({
                isAllSelectedArm: checked,
                idForArm: (checked ? plainOptionsArm : [])
            });

        }

    }
    handleAddData() {
        this.setState({
            showModal: true,
            showModalClass: 'show'
        })
    }
    handleFocusCloseModal() {
        this.setState({
            showModal: false,
            showModalClass: ''
        })
    }
    handleAddList() {
        // let url = api.url + api.getList + '?api_token=' + this.state.userData.API_Token + '&user_type=' + this.state.userData.label + '&user_type_to_get_data=SP';

        if (this.state.showModal) {
            return <div className="container">
                <div className="form-group">
                    <label>Client Name:</label>
                    <Dropdown
                        className="form-control"
                        placeholder="Select Client Name"
                        selection
                        options={client.map((a, b) => ({
                            text: a,
                            value: b
                        }))}
                        onChange={this.handleChangeReviewersAmount}
                    />
                </div>
                <div className="form-group">
                    <label>User Type:</label>
                    <label className="form-control">{this.state.usertype}</label>
                </div>
                <div className="form-group">
                    <label>First Name:</label>
                    <input type="text" name="fname" value={this.state.fname} onChange={this.handleChangeValue} className="form-control" />
                </div>
                <div className="form-group">
                    <label>Last Name:</label>
                    <input type="text" name="lname" value={this.state.lname} onChange={this.handleChangeValue} className="form-control" />
                </div>
                <div className="form-group">
                    <label>User Name:</label>
                    <input type="text" name="username" value={this.state.username} onChange={this.handleChangeValue} className="form-control" />
                </div>
                <div className="form-group">
                    <label>Password:</label>
                    <input type="password" name="password" value={this.state.password} onChange={this.handleChangeValue} className="form-control" />
                </div>
                <div className="form-group">
                    <button className="btn btn-primary" onClick={this.handleAddClientSubmit}>Submit</button>
                </div>

            </div>
        }
    }
    handleChangeReviewersAmount(event, data) {
        var value = data.value;
        let options = [];
        data.options.map((data, id) => {
            if (value === data.value) {
                options = data.text;
            }
        })
        this.setState({ clientList: options }, () => {
          
        });
    }
    handleChangeValue(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value }, () => {
        
        });
    }
    handleAddClientSubmit(e) {
        e.preventDefault();
     
        if (this.state.fname != '' && this.state.lname != '' && this.state.username != '' && this.state.password != '' && this.state.clientList != null) {
            var formData = new FormData();
            formData.append("api_token", this.state.userData.API_Token);
            formData.append("user_id", this.state.userData.USERID);
            formData.append("Client", this.state.clientList);
            formData.append("UserType", "Client Manager");
            formData.append("Login", this.state.username);
            formData.append("Password", this.state.password);
            formData.append("FirstName", this.state.fname);
            formData.append("LastName", this.state.lname);
            let url = api.url + api.addUser
            axios.post(url, formData, {
                headers: { 'Content-Type': 'application/json' }
            }).then(response => {
                if (response.data.status) {
                    alert(response.data.message)
                    this.handelList();
                    if (response.data.message === "User Added Successfull") {
                        setTimeout(() => {
                            this.handleCloseModal();
                        }, 1000);
                    }
                } else {
                    alert(response.data.message)
                }
            }).catch(error => {
            });

        } else {
            alert("Must Fill All Value");
        }
    }
    handleDelete(data) {
        this.handelList();
        let id1 = [];
        this.setState({ id: id1 });
    }
    render() {
        var tableHeadwithColumns;
        let column = this.props.columns;
        if (!(column.length === 0)) {
            tableHeadwithColumns = column.map(function (data) {

                const value = data.split('_').join(' ');
                if (!(value === 'UserID' || value === 'ClientID' || value === 'UserTypeID' || value === 'Label')) {
                    return <th key={value.toString()}>{value}</th>
                }
            });
        }
        return (
            <div>
                <Header activeClass="clientmanager" />
                <div className="wrapper">
                    <div className="container-fluid">
                        {/* <!-- Page-Title --> */}
                        <div className="row">
                            <div className="col-xl-6 col-sm-6">
                                <div className="page-title-box">
                                    <h4 className="page-title">Client Manager</h4>
                                </div>
                            </div>
                            <FilterForList inputVal={this.state.id}
                                deleteData={this.handleDelete.bind(this)} />
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-xl-12">
                                        <div className="card m-b-20">
                                            <div className="card-body">
                                                <h4 className="mt-0 m-b-10 header-title float-left">Client Manager</h4>
                                                <div className="right-blk-div">
                                                    <div>
                                                        <button className="btn sbtn" onClick={this.handleAddData}>Add</button>
                                                    </div>
                                                </div>
                                                <div className="table-responsive">
                                                    <table className="fixed_header table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    <div className="checkbox-wrapper">
                                                                        <input id="chk20" checked={this.state.isAllSelectedFrm} onChange={(e) => this.onCheckBoxChange('all', e.target.checked, 'frm')} type="checkbox" />
                                                                        <label htmlFor="chk20" className="toggle"></label>
                                                                    </div>
                                                                </th>
                                                                {tableHeadwithColumns}
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {this.generateRows(this.props.client)}

                                                        </tbody>
                                                    </table>


                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal className={`${this.state.showModalClass}`} show={this.state.showModal} onHide={this.handleFocusCloseModal}>
                    <Modal.Header className="modal-header1" closeButton>
                        <Modal.Title className="title-files">Add Client Manager</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="modal-new1">
                            <div className="row">
                                <div className="col-xl-12">
                                    <div className="card m-b-20">
                                        <div className="card-body">
                                            {this.handleAddList()}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.handleFocusCloseModal}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>

        );
    }
}
function mapStateToProps(state) {
    const { client } = state.listData;
    const { columns } = state.listData;
    return {
        client,
        columns
    };
}
export default connect(mapStateToProps)(clientManager);
