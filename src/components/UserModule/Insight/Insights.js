import React, { Component } from 'react';
import Header from '../../navigation/Header';
import Footer from '../../navigation/Footer';
import FusionCharts from 'fusioncharts/core';
import scrollcolumn2d from 'fusioncharts/viz/scrollcolumn2d';
import FusionTheme from 'fusioncharts/themes/es/fusioncharts.theme.fusion';
import ReactFC from 'react-fusioncharts';
import html2canvas from 'html2canvas';
import DonutChart from 'react-donut-chart';
import BarChart from 'react-d3-components/lib/BarChart';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import CanvasJSReact from '../../../canvasjs.react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import api from '../../../helpers/api';
import pieDataAction from '../../../actions/Insight/pieAction';
import stackChart from '../../../actions/Insight/stackChart';
import DayPicker from 'react-day-picker';
import {connect} from 'react-redux';
let imgData1;
ReactFC.fcRoot(FusionCharts, scrollcolumn2d, FusionTheme);
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var data = [];
var options;
let list = [];
let breakStack = [];
let forcedMatchedStack = [];
let submissionStack = [];
class Insights extends Component {
    constructor() {
        super();
        this.state = {
            userData: JSON.parse(sessionStorage.getItem('userData')),
            selectedDay: new Date(),
            isEmpty: true,
            isDisabled: false,
            pieData:[],
            loading:false,
            loadingForStack:false,
            stackData:[],
            selectedDayForStack : new Date()
        }
        this.toggleDataSeries = this.toggleDataSeries.bind(this);
        this.handleDayChange = this.handleDayChange.bind(this);
        this.handleDayChangeForStack = this.handleDayChangeForStack.bind(this);
        
    }
    handlePieChart = () => {
        setTimeout(() => {
            var selectedDay = new Date(this.state.selectedDay);
            var compareDay = new Date(this.state.selectedDay);
            var today = new Date();
         
            compareDay = compareDay.getFullYear() + '-' + (compareDay.getMonth()+1) + '-' + (compareDay.getDate());
            today = today.getFullYear() + '-' + (today.getMonth()+1) + '-' + (today.getDate());
            if(compareDay === today){
             
                selectedDay = selectedDay.getFullYear() + '-' + (selectedDay.getMonth()+1) + '-' + (selectedDay.getDate()-1);
            }else{
                selectedDay = selectedDay.getFullYear() + '-' + (selectedDay.getMonth()+1) + '-' + (selectedDay.getDate());    
            }     
            let url = api.url + api.getPieChartData + '?api_token=' + this.state.userData.API_Token + '&clientname=' + 'jpmam' + '&date=' + selectedDay;
            const { dispatch } = this.props;
            dispatch(pieDataAction(url));    
        }, 500);
   setTimeout(() => {
        this.handlePieChartData();
   }, 1000);
}
handlePieChartData(){
    var total = 0;
    var per  = 0;
    list = this.props.getdata;
    setTimeout(() => {
        this.setState({loading:true});    
    },1000);
    for (let i = 0; i < list.length; i++) {
    total = total + JSON.parse(list[i].RecordCount);
}
for (let i = 0; i < list.length; i++) {
    per = (JSON.parse(list[i].RecordCount) * 100 )/total;
    
   
    data.push({
        label:list[i].BreakStatus,
        value:per
    });
}
}
handleStackChart = () => {
   setTimeout(() => {
    var selectedDay = new Date(this.state.selectedDayForStack);
    var compareDay = new Date(this.state.selectedDay);
    var today = new Date();
  
    compareDay = compareDay.getFullYear() + '-' + (compareDay.getMonth()+1) + '-' + (compareDay.getDate());
    today = today.getFullYear() + '-' + (today.getMonth()+1) + '-' + (today.getDate());
    if(compareDay === today){
        selectedDay = selectedDay.getFullYear() + '-' + (selectedDay.getMonth()+1) + '-' + (selectedDay.getDate()-1);
    }else{
        selectedDay = selectedDay.getFullYear() + '-' + (selectedDay.getMonth()+1) + '-' + (selectedDay.getDate());    
    } 
    let url = api.url + api.getStackChartData + '?api_token=' + this.state.userData.API_Token + '&clientname=' + 'jpmam' + '&date=' + selectedDay;   
       const { dispatch } = this.props;
       dispatch(stackChart(url));  
   }, 500);
       
 setTimeout(() => {
    this.handleStackData();            
 }, 1000);

        
    }
    handleStackData(){
            this.props.stackData.forEach(element => {
             
               if(element.BreakStatus === "Breaks"){
                        breakStack.push({
                            label:element.Date,
                            y:JSON.parse(element.RecordCount)
                        });
               }else if(element.BreakStatus === "ForcedMatch"){
                forcedMatchedStack.push({
                    label:element.Date,
                    y:JSON.parse(element.RecordCount)
                });
               }else if(element.BreakStatus === "Submission"){
                submissionStack.push({
                    label:element.Date,
                    y:JSON.parse(element.RecordCount)
                });
               }
            });       
       
             options = {
                animationEnabled: true,
                exportEnabled: true,
                // title: {
                // 	text: "Operating Expenses of ACME",
                // 	fontFamily: "verdana"
                // },
                axisY: {
                    title: "Record Count",
                    // prefix: "€",
                    // suffix: "k"
                },
                toolTip: {
                    shared: true,
                    reversed: true
                },
                legend: {
                    verticalAlign: "center",
                    horizontalAlign: "right",
                    reversed: true,
                    cursor: "pointer",
                    itemclick: this.toggleDataSeries
                },
                data: [
                {
                    type: "stackedColumn",
                    name: "Breaks",
                    showInLegend: true,
                    yValueFormatString: "#,###",
                    dataPoints:breakStack
                },
                {
                    type: "stackedColumn",
                    name: "ForcedMatch",
                    showInLegend: true,
                    yValueFormatString: "#,###",
                    dataPoints:forcedMatchedStack
                },
                {
                    type: "stackedColumn",
                    name: "Submission",
                    showInLegend: true,
                    yValueFormatString: "#,###",
                    dataPoints:submissionStack
                }]
            };
           
            setTimeout(() => {
                this.setState({loadingForStack:true});    
            },300);
     
    }
    componentDidMount(){
     
        this.handlePieChart();
        this.handleStackChart();
    }
    generatePdf() {
   
        const input = document.querySelector("#ch");
        html2canvas(input)
            .then((canvas) => {
               
                const imgData = canvas.toDataURL('image/png');
                const pdf = new jsPDF();
                pdf.text('Reg-X', 10, 10);
                pdf.addImage(imgData, 'PNG', 10, 20, 200, 100);
                pdf.save("download.pdf");
            });

    }
    toggleDataSeries(e){
		if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
			e.dataSeries.visible = false;
		}
		else{
			e.dataSeries.visible = true;
		}
		this.chart.render();
    }
    handleDayChange(selectedDay, modifiers, dayPickerInput) {
        this.setState({loading:false});
        const input = dayPickerInput.getInput();
        this.setState({
          selectedDay,
          isEmpty: !input.value.trim(),
          isDisabled: modifiers.disabled === true,
        })
        data = [];
        list = [];
        this.handlePieChart();
        setTimeout(() => {
            this.setState({loading:true});           
        }, 1000);
    }
    handleDayChangeForStack(selectedDayForStack, modifiers, dayPickerInput) {
        this.setState({loadingForStack:false});
        const input = dayPickerInput.getInput();
        this.setState({
          selectedDayForStack,
          isEmpty: !input.value.trim(),
          isDisabled: modifiers.disabled === true,
        })
        breakStack = [];
        forcedMatchedStack = [];
        submissionStack = [];
        this.handleStackChart();
        setTimeout(() => {
            this.setState({loadingForStack:true});           
        }, 1000);
    }
    render() {
       
        var chartConfigs = {
            type: 'scrollcolumn2d', // The chart type
            width: '740', // Width of the chart
            height: '300', // Height of the chart
            dataFormat: 'json', // Data type
            dataSource: this.state.dataSource
        }
        const { selectedDay,selectedDayForStack, isDisabled, isEmpty } = this.state;
        return (
            <div id='ch1'>
                <Header activeClass="insights" />
                <div className="wrapper">
                    <div className="container-fluid">
                        {/* <!-- Page-Title --> */}
                        <div className="row">
                            <div className="col-sm-6 col-xl-6">
                                <div className="page-title-box">
                                    <h4 className="page-title">Insights</h4>
                                </div>
                            </div>


                            <div className="col-sm-6 col-xl-6 pull-right">
                                <div className="right-blk-div">
                                    <select>
                                        <option>Firm-ARM Recs insight</option>
                                        <option>Firm-ARM-NCA Recs Insight</option>
                                        <option>ARM-FIRDS ISINs Insight</option>
                                        <option>ARM-Vendor Instrument Data Insight</option>
                                        <option>ARM-Vendor Pricing Data Insight</option>
                                        <option>ARM-Firm-Personnel Data Insight</option>
                                    </select>
                                </div>
                                <div className="right-blk-div">
                                    <div>
                                        <span>Download:
                                     <a href="#" onClick={this.generatePdf.bind(this)}><i className="fas fa-file-pdf"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="container-fluid" id='ch'>

                        <div className="row">
                            <div className="col-xl-4">
                                <div className="card m-b-20">
                                    <div className="card-body">
                                    {this.state.loading === true?
                                    <div>
                                       <h4 className="mt-0 header-title  float-left">Latest Rec Stats</h4>
                                  
                                       <DayPickerInput
                                        value={selectedDay}
                                        onDayChange={this.handleDayChange}
                                        dayPickerProps={{
                                            selectedDays: selectedDay,
                                        }}
                                    />
                                 
                                        <div className="row text-center m-t-20">
                                           {this.props.getdata.map((element,key) =>  {
                                               return <div className="col-6" key={key}>
                                            <h5 className="">{element.RecordCount}</h5>
                                            <p className="text-muted">{element.BreakStatus}</p>
                                            </div>
                                           })}
                                        </div>
                                        <div>
                                            <DonutChart legend={false}
                                                innerRadius={0.70}
                                                outerRadius={0.90}
                                                height={300}
                                                width={300}
                                                clickToggle={false}
                                                formatValues={(values) => `${(values)}%`}
                                                colors={['#f0f1f4', '#28bbe3']}
                                                colorFunction={(colors, index) => colors[(index % colors.length)]}
                                                data={data}
                                             />
                                        </div>
                                        </div>
                                         :
                                         <div className="abs_loader"> <div className="loader"></div></div>
                                     }
                                   </div>
                                   
                                </div>
                            </div>
                            <div className="col-xl-8">
                                <div className="card m-b-20">
                                    <div className="card-body">
                                    {this.state.loadingForStack === true?
                                    <div>
                                    <DayPickerInput
                                        value={selectedDayForStack}
                                        onDayChange={this.handleDayChangeForStack}
                                        dayPickerProps={{
                                            selectedDays: selectedDayForStack,
                                        }}
                                        style={{float:'center'}}
                                    />
                                        {/* <h4 className="mt-0 header-title">Weekly trend of tickets received, resolved and unresovled</h4> */}
                                        {/* <div className="row text-center m-t-20">
                                            <div className="col-4">
                                                <h5 className="">$ 89425</h5>
                                                <p className="text-muted">Marketplace</p>
                                            </div>
                                            <div className="col-4">
                                                <h5 className="">$ 56210</h5>
                                                <p className="text-muted">Total Income</p>
                                            </div>
                                            <div className="col-4">
                                                <h5 className="">$ 8974</h5>
                                                <p className="text-muted">Last Month</p>
                                            </div>
                                        </div> */}

                                        <div>
                                        <CanvasJSChart options = {options}
                                            onRef={ref => this.chart = ref}
                                        />
                                        </div>
                                        {/* <div id="morris-bar-example" className="morris-chart-height"></div> */}
                                        {/* <Bar
                                            className="quizeChart"
                                            data={chartData}
                                            height={200}
                                            options={this.chartOptions}
                                        /> */}
                                    </div>
                                    :  <div className="abs_loader"> <div className="loader"></div></div>}
                                </div>
                                </div>
                            </div>

                        </div>
                        {/* <!-- end row --> */}

                    </div>
                    {/* <!-- end container-fluid --> */}
                </div>
                {/* <!-- end wrapper --><!-- Footer --> */}
                <Footer />
            </div>


        )
    }

}
function mapStateToProps(state) {
    const { getdata } = state.pieData
    const {stackData}  = state.stackData
    return {
        getdata,
        stackData
    };
}
export default connect(mapStateToProps)(Insights);