import React, { Component } from 'react';
import Header from '../../navigation/Header';
import Footer from '../../navigation/Footer';
import FilterForBackReporting from '../../navigation/FilterForBackReporting';
import moment from 'moment';
import Helmet from 'react-helmet';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import { formatDate, parseDate } from 'react-day-picker/moment';
import api from '../../../helpers/api';
import axios from 'axios';
import { connect } from 'react-redux';
import backReportingAction from '../../../actions/Back Reporting/backReportingAction';
import { CSVLink } from "react-csv";
const dataMsg = {
    width: '1000px',
    color: '#5b626b',
    backgroundColor: '#ffffff',
    border: '0px'
}
let ckdArr = [];
let ckdArrForArm = [];
let plainOptionsFrm = [];
let plainOptionsArm = [];
let uniqueNames = [];
let uniqueNames2 = [];
let ckAdrrNewArm = [];
let ckAdrrNew = [];
class BackReporting extends Component {
    constructor(props) {
        super(props);
        var tempDate = new Date();
        var firstDay = new Date(tempDate.getFullYear(), tempDate.getMonth(), 1);
        var tempDate1 = new Date();
        var date = tempDate1.getFullYear() + '-' + (tempDate1.getMonth() + 1) + '-' + tempDate1.getDate();
        var importdate = new Date();

        this.state = {
            from: new Date(),
            to: new Date(),
            date: date,
            userData: JSON.parse(sessionStorage.getItem('userData')),
            backReportingData: [],
            transpose: '',
            id: [],
            isAllSelectedFrm: false,
            dataForFile: [],
            ShowIsEdited: '',
            alltrancation: '',
            allTransactionValue: false,
            loading: false,
            refresh: false

        };
        this.handleToChange = this.handleToChange.bind(this);
        this.handleFromChange = this.handleFromChange.bind(this);
        this.handleTrans = this.handleTrans.bind(this);
        this.handleSingleCheckBox = this.handleSingleCheckBox.bind(this);
        this.handleDowndloadFile = this.handleDowndloadFile.bind(this);
        this.handleTransaction = this.handleTransaction.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
        this.handleLoder = this.handleLoder.bind(this);
    }


    handleRefresh(RefreshValue) {
        this.setState({loading:false});
        this.setState({ refresh: RefreshValue }, () => {
       
        });
        this.handelList(0);
        setTimeout(() => {
            this.setState({ loading: true });
        }, 2000);
    }
    handleLoder() {
        return <div className="abs_loader"> <div className="loader"></div></div>
    }

    handelList = (ShowIsEdited) => {
       
        if (ShowIsEdited === 0) {
            ShowIsEdited = '';

        }
      
        var from = new Date(this.state.from);
       
        from = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        var to = new Date(this.state.to);
        to = to.getFullYear() + '-' + (to.getMonth() + 1) + '-' + to.getDate();
        let url = api.url + api.getBackReportingData + '?api_token=' + this.state.userData.API_Token + '&clientName=' + this.state.userData.ClientName + '&FileFromDate=' + from + '&FileToDate=' + to + '&ShowIsEdited=' + ShowIsEdited;
        const { dispatch } = this.props;
        dispatch(backReportingAction(url));
    
    }
    componentWillMount() {
        this.setState({ loading: false });
       
        this.handelList(0);
        setTimeout(() => {
            this.setState({ loading: true });
        }, 2000);
        
    }
    showFromMonth() {
        const { from, to } = this.state;
        if (!from) {
            return;
        }
        if (moment(to).diff(moment(from), 'months') < 2) {
            this.to.getDayPicker().showMonth(from);
        }
    }
    handleFromChange(from) {
        // Change the from date and focus the "to" input field
        this.setState({ from });
    }
    handleToChange(to) {
        this.setState({ to }, this.showFromMonth);
        this.handelList(0);
    }
    columnsgetData(getColumnData) {
        let columns = [];
        let col = "";
        Object.keys(getColumnData).map(function (data, number) {
            if(data == "FirmDataID" || data == "IMPORT_DATE"){
                col = <td className="hide_firmDataId" key={number}>{getColumnData[data]}</td>
            }else{
                col = <td key={number}>{getColumnData[data]}</td>
            }
            columns.push(col)
        });
        return columns;
    }
    generateRows = (backReporting) => {
        const rows = [];
        for (let i = 0; i < backReporting.length; i++) {
            let checked = false;
            if (this.state.id.indexOf(`column_${backReporting[i].FirmDataID}`) >= 0 || this.state.isAllSelectedFrm) {
                checked = true;
            }
            plainOptionsFrm[i] = (`column_${backReporting[i].FirmDataID}`);
            rows.push(
                <tr key={`row_${i}`} >
                    <td> <div className="checkbox-wrapper"><input id={`column_${backReporting[i].FirmDataID}`} checked={checked} name="checkedAll" onChange={this.handleSingleCheckBox} type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                    {this.columnsgetData(backReporting[i])}
                </tr>
            );
        }

        return rows;
    }
    handleSingleCheckBox(e) {
        ckAdrrNew = [];
        ckAdrrNew = this.state.id;
        //remove parts declaration which is not used by krupal 20 dec
        if (this.state.isAllSelectedFrm) {
            ckAdrrNew = plainOptionsFrm;
            var ckAdrrNewChecked = ckAdrrNew.filter(function (ele) {
                if (ele !== e.target.id) {
                    return ele;
                }

            });
        } else {

            if (e.target.checked) {
                const parts = e.target.id.split("_");
                ckAdrrNew[parts[1]] = e.target.id;
                ckAdrrNewChecked = ckAdrrNew
            } else {
                ckAdrrNewChecked = ckAdrrNew.filter(function (ele) {
                    if (ele !== e.target.id) {
                        return ele;
                    }

                });
            }
        }
        ckdArr = ckAdrrNewChecked;
        uniqueNames2 = Array.from(new Set(ckdArr));
        //add condition for remove undefined by krupal 20 dec
        var index = uniqueNames2.indexOf(undefined);
        if (index > -1) {
            uniqueNames2.splice(index, 1);
        }
        this.setState({ id: uniqueNames2, isAllSelectedFrm: false }, () => {
          
        });
    }
    onCheckBoxChange(checkName, isChecked, type) {
        const checked = isChecked;
        if (type === 'frm') {
            this.setState({
                isAllSelectedFrm: checked,
                id: (checked ? plainOptionsFrm : [])
            });

        }
        if (type === 'arm') {
            this.setState({
                isAllSelectedArm: checked,
                idForArm: (checked ? plainOptionsArm : [])
            });

        }

    }
    handleTrans(transValue) {
        this.setState({ transpose: transValue });
     
    }
    generateRowsWhenTranspose() {

        if (this.state.transpose) {
            let columns = this.props.columns;
            if (!(columns.length === 0)) {
                const rows = [];
                rows.push(
                    //Add checkbox type by krupal by 20 dec
                    <tr key={`row_`}>
                        <th ><div className="checkbox-wrapper"> <input id="chk20" type="checkbox" /><label htmlFor="chk20" className="toggle"></label></div></th>
                        {this.getanotherColumnTranspose(0, false)}
                    </tr>

                )
                for (let i = 0; i < columns.length; i++) {
                    const value = columns[i].split('_').join(' ');
                    const ckd = value.toString();
                    let row = "" ;
                    if (!(ckd === 'FirmDataId' || ckd === 'InstrumentBreak')) {
                        if(ckd  == "FirmDataID" || ckd  == "IMPORT_DATE" ){
                            row = <tr className="hide_firmDataId" key={ckd}><th>{value}</th>{this.getanotherColumnTranspose(columns[i], true)}
                            </tr>
                        }else{
                            row = <tr key={ckd}><th>{value}</th>{this.getanotherColumnTranspose(columns[i], true)}
                            </tr>
                        }
                        
                        rows.push(row);
                    }
                }
                return rows;
            }
        }
    }
    getanotherColumnTranspose(column, status) {
        // Add checkbox type  and condition by krupal 20 dec
        let columnName = column;
        let columns = [];
        var columnsRecouncilationmap = this.props.backReporting
        if (status) {
            for (let i = 0; i < columnsRecouncilationmap.length; i++) {

                columns.push(
                    <td key={`col_${i}`}>{columnsRecouncilationmap[i][columnName]}</td>
                )
            }
        } else {
            for (let i = 0; i < columnsRecouncilationmap.length; i++) {

                columns.push(
                    <td key={`row_${i}`}><div className="checkbox-wrapper"><input id={`column_${i}`} name="checkedAll" type="checkbox" /> <label htmlFor="chk20" className="toggle"></label></div></td>
                )
            }
        }
        return columns;
    }
    handleDowndloadFile() {
        const currentRecords = this.props.backReporting;
        let id = []; 
        id = this.state.id.map(id => id);
        var data_to_download = []
        if (id.length) {
            for (let i = 0; i < id.length; i++) {
                let record_to_download = {}
                for (let j = 0; j < currentRecords.length; j++) {
                    if (id[i] === `column_${currentRecords[j].FirmDataID}`) {
                        record_to_download = currentRecords[j]
                        delete record_to_download['IMPORT_DATE'];
                        delete record_to_download['FirmDataID'];
                        data_to_download.push(record_to_download);
                        
                    }
                }
            }
            this.setState({ dataForFile: data_to_download })   
        } else {
            alert('Plz Select Data');
        }
    }
    handleTransaction(transactionValue) {
        this.setState({ allTransactionValue: transactionValue }, () => {
         
        });
      
        if (transactionValue === true) {
            setTimeout(() => {
                this.handelList(1);
            }, 1000)
        } else {
            setTimeout(() => {
                this.handelList('');
            }, 1000)
        }
    }

    render() {
        const DataAvailableMsg = 'Transaction not available for this date, please use different date.';


        const { from, to } = this.state;
        const modifiers = { start: from, end: to };


        var tableHeadwithColumns;
        let column = this.props.columns;
        let col = "";
        if (!(column.length === 0)) {
            tableHeadwithColumns = column.map(function (data) {
                const value = data.split('_').join(' ');
                if (!(value === 'BreakStatus' || value === 'DataId')) {
                    
                    if(value == "FirmDataID" || value == "IMPORT DATE" ){
                        col =  <th className="hide_firmDataId" key={value.toString()}>{value}</th>
                    }else{
                        col = <th key={value.toString()}>{value}</th>
                    }
                    return col ;
                }
            });
        }
        console.log(this.state);
        return (
            <div>
                <Header activeClass="backreporting" />
                <div className="wrapper">
                    <div className="container-fluid">
                        {/* <!-- Page-Title --> */}
                        <div className="row">
                            <div className="col-xl-6 col-sm-6">
                                <div className="page-title-box">
                                    <h4 className="page-title">Reporting</h4>
                                </div>
                            </div>

                            <FilterForBackReporting ontransClick={this.handleTrans}
                            onRefreshClick={this.handleRefresh}
                                onTransactionclick={this.handleTransaction} />
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-xl-12">
                                        <div className="card m-b-20">
                                        {this.state.loading === true ?
                                            <div className="card-body">
                                                <h4 className="mt-0 m-b-10 header-title float-left"></h4>
                                                {/* <div className="right-blk-div">
                                    <div>
                                        <span>Download: 
                                     <a href="#"><i className="fas fa-file-pdf"></i></a>
                                        </span>
                                    </div>
                                </div> */}
                                                <div className="right-blk-div">
                                                    <div>
                                                        {this.state.id.length ?
                                                            <CSVLink data={this.state.dataForFile} onClick={this.handleDowndloadFile}>
                                                                <span>Download: <i className="fas fa-file-excel"></i>
                                                                    <a href="#" onClick={this.handleDowndloadFile}></a>
                                                                </span>
                                                            </CSVLink>
                                                            : null}
                                                    </div>

                                                </div>
                                                <div className="InputFromTo">
                                                    <DayPickerInput
                                                        value={from}
                                                        placeholder="From"
                                                        format="LL"
                                                        formatDate={formatDate}
                                                        parseDate={parseDate}
                                                        dayPickerProps={{
                                                            selectedDays: [from, { from, to }],
                                                            disabledDays: { after: to },
                                                            toMonth: to,
                                                            modifiers,
                                                            numberOfMonths: 2,
                                                            onDayClick: () => this.to.getInput().focus(),
                                                        }}
                                                        onDayChange={this.handleFromChange}
                                                    />
                                                    <span className="InputFromTo-to">
                                                        <DayPickerInput
                                                            ref={el => (this.to = el)}
                                                            value={to}
                                                            placeholder="To"
                                                            format="LL"
                                                            formatDate={formatDate}
                                                            parseDate={parseDate}
                                                            dayPickerProps={{
                                                                selectedDays: [from, { from, to }],
                                                                disabledDays: { before: from },
                                                                modifiers,
                                                                month: from,
                                                                fromMonth: from,
                                                                numberOfMonths: 2,
                                                            }}
                                                            onDayChange={this.handleToChange}
                                                        />
                                                    </span>
                                                    <Helmet>
                                                        <style>{`

                                        .InputFromTo { margin:0 0 10px 25%; float:left; padding:0; text-align:center;  display:flex;}
                                        .DayPickerInput-OverlayWrapper { font-size:12px; left:0; right:auto;}
                                        .InputFromTo .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
                                            background-color: #f0f8ff !important;
                                            color: #4a90e2; font-size:12px;
                                        }
                                        .InputFromTo .DayPicker-Day {
                                            border-radius: 0 !important; font-size:12px;
                                        }
                                        .InputFromTo .DayPicker-Day--start {
                                            border-top-left-radius: 50% !important; font-size:12px;
                                            border-bottom-left-radius: 50% !important;
                                        }
                                        .InputFromTo .DayPicker-Day--end {
                                            border-top-right-radius: 50% !important; font-size:12px;
                                            border-bottom-right-radius: 50% !important;
                                        }
                                        .InputFromTo .DayPickerInput-Overlay {
                                            width: 520px; font-size:12px;
                                        }
                                        .InputFromTo-to .DayPickerInput-Overlay {
                                            margin-left: 0;
                                        }
                                        `}</style>
                                                    </Helmet>
                                                </div>
                                                {this.state.transpose && this.props.backReporting.length > 0 ?
                                                    <div className="table-responsive">
                                                        {this.props.backReporting.length ?
                                                            <table className="fixed_header table table-striped table-bordered table-hover">
                                                                <tbody>
                                                                    {this.generateRowsWhenTranspose()}
                                                                </tbody>
                                                            </table>
                                                            : <div>
                                                                <table className="fixed_header table table-striped table-bordered table-hover">
                                                                    <thead>
                                                                    </thead>

                                                                    <tbody>
                                                                        <tr>
                                                                            <th style={dataMsg}><h4 className="mt-0 m-b-10 header-title float-left">{DataAvailableMsg}</h4></th>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>}
                                                    </div>
                                                    :
                                                    <div className="table-responsive">
                                                        {this.props.backReporting.length ?
                                                            <table className="fixed_header table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <div className="checkbox-wrapper">
                                                                                <input id="chk20" checked={this.state.isAllSelectedFrm} onChange={(e) => this.onCheckBoxChange('all', e.target.checked, 'frm')} type="checkbox" />
                                                                                <label htmlFor="chk20" className="toggle"></label>
                                                                            </div>
                                                                        </th>
                                                                        {tableHeadwithColumns}
                                                                    </tr>

                                                                </thead>
                                                                <tbody>
                                                                    {this.generateRows(this.props.backReporting)}
                                                                </tbody>
                                                            </table>
                                                            : <div>
                                                                <table className="fixed_header table table-striped table-bordered table-hover">
                                                                    <thead>
                                                                    </thead>

                                                                    <tbody>
                                                                        <tr>
                                                                            <th style={dataMsg}><h4 className="mt-0 m-b-10 header-title float-left">{DataAvailableMsg}</h4></th>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>}
                                                    </div>
                                                }
                                            </div> :
                                            <div>
                                            {this.handleLoder()}
                                        </div>
                                        }
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}
function mapStateToProps(state) {
    const { backReporting } = state.backReportingData;
    const { columns } = state.backReportingData;
    return {
        backReporting,
        columns
    };
}
export default connect(mapStateToProps)(BackReporting);
