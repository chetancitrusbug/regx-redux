import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Header from '../../navigation/Header';
import Footer from '../../navigation/Footer';
import FilterForImportData from '../../navigation/FilterForImportData';
// import { Line, Bar } from 'react-chartjs-2';
import axios from 'axios'
import { connect } from 'react-redux'
import api from '../../../helpers/api';
import moment from 'moment';
import Helmet from 'react-helmet';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import dataImportAction from '../../../actions/Data Import/dataImportAction';
import { formatDate, parseDate } from 'react-day-picker/moment';
import {
    FilteringState,
    IntegratedFiltering,
} from '@devexpress/dx-react-grid';

import {
    Grid,
    Table,
    TableHeaderRow,
    TableFilterRow,
} from '@devexpress/dx-react-grid-bootstrap4';

let url;
class DataImport extends Component {
    constructor() {
        super();

        var tempDate = new Date();
        var firstDay = new Date('2018', tempDate.getMonth(), 1);
       // var firstDay = new Date();
        this.state = {
            getData: [],
            userData: JSON.parse(sessionStorage.getItem('userData')),
            loading: false,
            from: firstDay,
            to: new Date(),
            dataLoading: false,
            transpose: false,
            refresh: false,
            columns: [
                { name: "FileName", title: "File Name" },
                { name: "SourceName", title: "Source" },
                { name: "FileTypeName", title: "File Type" },
                { name: "NoOfRowsInFile", title: "No. of Rows" },
                { name: "NoOfRowsInserted", title: "No. of Rows Inseted" },
                { name: "ProcessingStatus", title: "Processing Status" },
                { name: "UserName", title: "UserName" },
                { name: "FileImportDateTime", title: "Day Time" },
                { name: "ReasonCode", title: "Reason Code" },

            ],
            rows: [],
        };

        this.handleFromChange = this.handleFromChange.bind(this);
        this.handleToChange = this.handleToChange.bind(this);
        this.handleTrans = this.handleTrans.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
        this.handleLoder = this.handleLoder.bind(this);
    }
    handleLoder() {
        return <div className="abs_loader"> <div className="loader"></div></div>
    }

    handelList = () => {
        this.setState({ dataLoading: false });
        var from = new Date(this.state.from);
        from = from.getFullYear() + '-' + (from.getMonth() + 1) + '-' + from.getDate();
        var to = new Date(this.state.to);
        to = to.getFullYear() + '-' + (to.getMonth() + 1) + '-' + to.getDate();

        url = api.url + api.dataImport + '?&clientId=' + this.state.userData.ClientName + '&from_date=' + from + '&to_date=' + to + '&api_token=' + this.state.userData.API_Token;
        const { dispatch } = this.props;
        dispatch(dataImportAction(url));
        //this.setState({ rows : this.props.getData})
    }
    showFromMonth() {
        const { from, to } = this.state;
        if (!from) {
            return;
        }
        if (moment(to).diff(moment(from), 'months') < 2) {
            this.to.getDayPicker().showMonth(from);
        }
    }
    handleFromChange(from) {
        // Change the from date and focus the "to" input field
        this.setState({ from });
        //this.handelList()
    }
    handleToChange(to) {
        this.setState({ to }, this.showFromMonth);
        this.handelList()

    }
    componentWillMount() {

        this.setState({ loading: false });
       
        this.handelList()
        setTimeout(() => {
            this.setState({ loading: true });
            const classList = ReactDOM.findDOMNode(this).querySelector('.table').classList;
            classList.add('table-bordered');
            classList.add('table-striped');
            classList.add('table-condensed');
            classList.add('fixed_header');
            
        }, 2000);
       
    }

    
    columnsgetData(getColumnData) {
        let columns = [];

        Object.keys(getColumnData).map(function (data, number) {
            columns.push(<td key={number}>{getColumnData[data]}</td>)
        });
        return columns;
    }

    generateRows = (getData) => {
        const rows = [];
        for (let i = 0; i < getData.length; i++) {
            rows.push(
                <tr key={`row_${i}`} >
                    {this.columnsgetData(getData[i])}
                </tr>
            );
        }
        return rows;
    }
    handleTrans(transValue) {
        this.setState({ transpose: transValue });
    }
    getanotherColumnTranspose(columns) {
        // Add checkbox type  and condition by krupal 20 dec
        let columnName = [];
        columnName = columns;
       
        let column = [];
        var columnsRecouncilationmap = this.props.getdata
        for (let i = 0; i < columnsRecouncilationmap.length; i++) {
         
            column.push(
                <td>{columnsRecouncilationmap[i][columnName]}</td>
            );
        }
        return column;
    }

    generateRowsWhenTranspose() {

        if (this.state.transpose) {
            let columns = [];
            const rows = [];
            columns.push('FileName');
            columns.push('SourceName');
            columns.push('FileTypeName');
            columns.push('NoOfRowsInFile');
            columns.push('NoOfRowsInserted');
            columns.push('ProcessingStatus');
            columns.push('ReasonCode');
            columns.push('UserName');
            columns.push('FileImportDateTime');

            const colforDisplay = [];
            colforDisplay.push('File Name');
            colforDisplay.push('Source');
            colforDisplay.push('File Type');
            colforDisplay.push('No. of Rows');
            colforDisplay.push('No. of Rows Inserted');
            colforDisplay.push('Processing Status');
            colforDisplay.push('Reason Code');
            colforDisplay.push('UserName');
            colforDisplay.push('Day Time');

            for (let i = 0; i < columns.length; i++) {
                const value = columns[i].split('_').join(' ');
                const ckd = value.toString();
                rows.push(
                    <tr key={ckd} >
                        <th>{colforDisplay[i]}</th>
                        {this.getanotherColumnTranspose(columns[i])}
                    </tr>

                );
            }

            return rows;
        }
    }
    handleRefresh(RefreshValue) {
        this.setState({loading:false});
        this.setState({ refresh: RefreshValue }, () => {
       
        });
        this.handelList();
        setTimeout(() => {
            this.setState({ loading: true });
        }, 2000);
    }
    render() {
        // const { fromdate, enddate } = this.state;
        // const modifiers = { start: fromdate, end: enddate };
        const { rows, columns } = this.state;
        const { from, to } = this.state;
        const modifiers = { start: from, end: to };
        return (
            <div >
                <Header activeClass="data-import" />
                <div className="wrapper">
                    <div className="container-fluid">
                        {/* <!-- Page-Title --> */}
                        <div className="row">
                            <div className="col-xl-6 col-sm-6">
                                <div className="page-title-box">
                                    <h4 className="page-title">Data Import</h4>
                                </div>
                            </div>
                            <FilterForImportData
                                ontransClick={this.handleTrans}
                                onRefreshClick={this.handleRefresh}
                            />
                        </div>
                    </div>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-xl-12">
                                <div className="card m-b-20">
                                    {this.state.loading === true ?
                                        <div className="card-body">
                                            <h4 className="mt-0 m-b-10 header-title float-left"></h4>
                                            <div className="InputFromTo">
                                                <DayPickerInput
                                                    value={from}
                                                    className="date-input"
                                                    placeholder="From"
                                                    format="LL"
                                                    formatDate={formatDate}
                                                    parseDate={parseDate}
                                                    dayPickerProps={{
                                                        selectedDays: [from, { from, to }],
                                                        disabledDays: { after: to },
                                                        toMonth: to,
                                                        modifiers,
                                                        numberOfMonths: 2,
                                                        onDayClick: () => this.to.getInput().focus(),
                                                    }}
                                                    onDayChange={this.handleFromChange}
                                                />
                                                <span className="InputFromTo-to">
                                                    <DayPickerInput
                                                        ref={el => (this.to = el)}
                                                        value={to}
                                                        className="date-input"
                                                        placeholder="To"
                                                        format="LL"
                                                        formatDate={formatDate}
                                                        parseDate={parseDate}
                                                        dayPickerProps={{
                                                            selectedDays: [from, { from, to }],
                                                            disabledDays: { before: from },
                                                            modifiers,
                                                            month: from,
                                                            fromMonth: from,
                                                            numberOfMonths: 2,
                                                        }}
                                                        onDayChange={this.handleToChange}
                                                    />
                                                </span>
                                                <Helmet>
                                                    <style>{`

                                        .InputFromTo { margin:0 0 10px 25%; float:left; padding:0; text-align:center;  display:flex;}
                                        .DayPickerInput-OverlayWrapper { font-size:12px; left:0; right:auto;}
                                        .InputFromTo .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
                                            background-color: #f0f8ff !important;
                                            color: #4a90e2; font-size:12px;
                                        }
                                        .InputFromTo .DayPicker-Day {
                                            border-radius: 0 !important; font-size:12px;
                                        }
                                        .InputFromTo .DayPicker-Day--start {
                                            border-top-left-radius: 50% !important; font-size:12px;
                                            border-bottom-left-radius: 50% !important;
                                        }
                                        .InputFromTo .DayPicker-Day--end {
                                            border-top-right-radius: 50% !important; font-size:12px;
                                            border-bottom-right-radius: 50% !important;
                                        }
                                        .InputFromTo .DayPickerInput-Overlay {
                                            width: 520px; font-size:12px;
                                        }
                                        .InputFromTo-to .DayPickerInput-Overlay {
                                            margin-left: 0;
                                        }
                                        `}</style>
                                                </Helmet>
                                            </div>   
                                            
                                                 {this.state.transpose ?
                                                <div className="table-responsive">
                                                    <table className="fixed_header table table-striped table-bordered table-hover">
                                                        <tbody>
                                                            {this.generateRowsWhenTranspose()}
                                                        </tbody>
                                                    </table>
                                                </div>
                                                
                                                :
                                                <div className="table-responsive">
                                                    <Grid
                                                        rows={this.props.getdata}
                                                        columns={columns}
                                                    >
                                                        <FilteringState defaultFilters={[]} />
                                                        <IntegratedFiltering />
                                                        <Table />
                                                        <TableHeaderRow />
                                                        <TableFilterRow />
                                                    </Grid>
                                                    
                                                     {/* <table className="fixed_header table table-striped table-bordered table-hover">
                                                         <thead>
                                                             <tr>
                                                                 <th width="30%">File Name</th>
                                                                 <th width="15%">Source</th>
                                                                 <th width="10%">File Type</th>
                                                                 <th width="10%">No. of Rows</th>
                                                                 <th width="10%">No. of Rows Inseted</th>
                                                                 <th width="10%">Processing Status</th>
                                                                 <th width="15%">Reason Code</th>
                                                                 <th width="10%">UserName</th>
                                                                 <th width="10%">Day Time</th>
                                                            </tr>
                                                         </thead>
                                                        <tbody>
                                                             {this.generateRows(this.props.getdata)}
                                                         </tbody>
                                                    </table> */}
                                                
                                                 </div>
                                            }
                                        </div>
                                        : <div>
                                            {this.handleLoder()}
                                        </div>}
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                {/* <!-- end wrapper --><!-- Footer --> */}
                <Footer />
            </div >


        )
    }

}

function mapStateToProps(state) {
    const { getdata } = state.dataImport
   
    return {
        getdata,

    };
}
export default connect(mapStateToProps)(DataImport);