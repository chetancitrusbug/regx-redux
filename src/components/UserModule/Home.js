import React, { Component } from 'react';
import Header from '../navigation/Header';
import Footer from '../navigation/Footer';
class Home extends Component {
    constructor() {
        super();
        this.state = {
            login: true,
            userdetails: JSON.parse(sessionStorage.getItem('userData'))
        };
    }
    render() {

        return (
            <div>
                <Header activeClass="accuracyInstrument" />
                <div className="customerList">
                    {/* <a href="/upload" className="side-link">Upload File</a> */}

                    <h2>User Profile</h2>

                    <div className="col-md-12"><label><span>ClientID</span><p>{this.state.userdetails.ClientID}</p></label></div>
                    <div className="col-md-12"><label><span>CreateTimestamp</span><p>{this.state.userdetails.CreateTimestamp}</p></label></div>
                    <div className="col-md-12"><label><span>CreateUser</span><p>{this.state.userdetails.CreateUser}</p></label></div>
                    <div className="col-md-12"><label><span>ModifiedUser</span><p>{this.state.userdetails.ModifiedUser}</p></label></div>
                    <div className="col-md-12"><label><span>UserName</span><p>{this.state.userdetails.UserName}</p></label></div>
                </div>
                <Footer />
            </div>

        );
    }
};
export default Home;